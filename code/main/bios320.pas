{

  !BIOS by Bluefish and various other helpfull people...
  Check www.11a.nu for new versions or to join the development team.

  Sourcecode main file, BIOSxxx.PAS.
  Needs a lot of other modules (units and include files) in order to compile!

}

{$F+} { PROCEDURE must be SEGMENT:OFFSET adresses to be used as VAR PROCEDURE }

{ Define debugInit } { Wait for any key before loading GUI ?          }
{ Define SCk       } { Antivirus selfcheck; PROC11AI.PAS              }
{ Define NDM       } { No debug mode, disables all bp built in checks }
{ Define AlphaTest } { Display alpha-Test message?                    }
{ Define CheckOS   } { Warn if (e.g) WindowsNT is running?            }

{$IFDEF NDM}
  {$I-,D-,L-,R-,S-,B-,Y-,Q-}
{$ELSE}
  {$I+,D+,L+,R+,S+,B+,Y+,Q+}
{$ENDIF}

uses
    crt,dos,  { comes with borland pascal }
    apps,     { various usefull functions }
    cmos,     { cmos access functions     }
    biosdox,  { built-in documentation    }
    biosami,  { crackers, ami #1 #2 #3    }
    biosaw45, { crackers, award           }
    biosibm,  { crackers, ibm             }
    biosdtk,  { crackers, dtk             }
    biodump,  { ROM memory dumper         }
    biosed,   { CMOS Editor               }
    bioflame, { screensaver               }
    view11a2, { built-in viewer           }
    view11al, { built-in viewer           } { messy & badly coded ;-) }
    harddet2, { hardware & OS detection   }
    timetask  { simple taskswitching      }
    {$IFDEF SCk}
    ,proc11ai { simple selfcheck          }
    {$ENDIF};
const
     alphaVersion  = '20000810';
     alphaLastDate = 200106;

     iBiosVersion  = '3.20';
     iBios         = '!BIOS '+iBiosVersion;
     iBiosFull     = iBios+' (build '+alphaVersion+')';


var
 lsm       : word;                   { last screen mode }
 bioslogo  : string;                 { path+filename to BIOSLOGO.SCX }
 cmosArray : array [0..127] of byte; { temporary copy of cmos }

{$I BIOSSS   } { screen saver(s)           }
{$I BIOSPAYL } { various functions         }
{$I BIOSLOGO } { logo display & intro fade }
{$I BIOSCMD  } { command line interface    }

var biosenv : byte;
const
 envNoAV       = b00000001;
 envNoAlphaMsg = b00000010;
 envNoMultiChk = b00000100;
 envNoLogo     = b00001000;

procedure biosenv_notice(s : string);
begin
 writeLn ('NOTICE: ',s,' disabled by IBIOS environment.');
end;

procedure biosenv_set;
var i : integer;
    s : string;
begin
  s:=GetEnv('IBIOS');
  if s[0]<>#0 then begin
   Val(s, biosenv, i);
   if i<>0 then begin
     writeLn('Environment IBIOS found (=',s,'), but there was an error!');
     writeLn('String->Integer conversion failed at character ',i,' which is "',s[i],'"');
     if(readkey=#0) then readkey;
     biosenv:=0;
   end;
  end else biosenv:=0;
end;

procedure startup (s : string80);
begin
 writeLn ('[startup] '+s+'...');
end;


var
 b : byte;
 w : word;
 c : char;
 done : boolean;

begin
{----------------------------------------------------------------------}

if paramCount <> 0 then handleCommandPrompt;

startup ('Main program ('+iBiosFull+') loading');

biosenv_set;
randomize;

startup ('Saving current screen');
lsm := lastMode; { saves screen mode }
screenSaverInit(sptr);

{------------------------------------------- Self Check -----------}

{$IFDEF SCk}
if((biosenv AND envNoAV)=0) then begin
  startup ('Antivirus selfcheck');
  selfcheck;
end else
  biosenv_notice('AntiVirus selfcheck');
{$ENDIF}

{----------------------------------------------------------------------}

{$IFDEF AlphaTest}
if((biosenv AND envNoAlphaMsg)=0) then begin
 startup ('Alpha/development popup message');
 writeLn;
 writeLn('  '+iBios+' AlphaBuild: ', alphaVersion+rad);
 writeLn(
  '  This is a testversion, not a release.'+rad+
  '  Only intended for pre-release testing, debugging and evaluation.'+rad);
 if dateYYYYMM > alphaLastDate then begin
  writeLn (
   '  This version is too old. Please get a new one from:'+
   rad+website+rad+website);
  {halt(0);}
 end else writeLn ('  This version will expire ',alphaLastDate,'01');
 writeLn (rad+'  Press any key to continue');
 if readkey = #0 then readkey;
 writeLn;
end else
 biosenv_notice('Alpha version message');
{$ENDIF}

{----------------------------------------------------------------------}

startup ('Silencing PC speaker');
nosound;

with c1org do begin
 startup ('Saving VGA-DAC color ('+ abc( number, 0 ) +')');
 getVGA_Pal256 (number,red,blue,green);
end;

startup ('Executing Harddet2, hardware & OS detector');
harddet2init (true);
if not timetask.initilized then begin
 startup ('Detecting OS');
 timetask.init;
end;

{$IFDEF CheckOS}
if((biosenv AND envNoMultiChk)=0) then begin
 if task.os <> 0 then begin
  write (rad+rad+
  '  WARNING: '+rad+
  '  a multitasker was detected.'+rad+
  '  To ensure that hardware and memory is accessable, we recommend you use'+rad+
  '  dos without any secondary OS loaded! ... (esp. WinNT is really unfriendly!)'+rad+rad+
  '  Press any key to continue');
  if readkey=#0 then readkey;
  writeLn(rad);
 end;
end else
 biosenv_notice('WinNT/multitasker detection');
{$ENDIF}

if((biosenv AND envNoLogo)=0) then begin
 startup ('Searching for rixlogo');
 bioslogo := exepath+'BIOSLOGO.SCX';
 if fileExists(bioslogo) then begin
  startup ('Displaying logo');
  nicefadeintro;
  displayMCGAlogo;
 end else begin
  startup ('No rixlogo, you''ll have to live without graphics');
 end;
end else
 biosenv_notice('MCGA Logo & Fade');

if getEnv('IBIOSDEBUG')<>'' then begin
  writeLn;
  writeLn('Environment IBIOSDEBUG set, a development "feature" ;-)');
  writeLn('Issue "SET IBIOSDEBUG=" in DOS to disable...');
  write ('Press any key to contiune');
  if readkey=#0 then readkey;
  writeLn(rad);
end;



startup ('Common api''s initialisation complete, initialising GUI');

{------------------------------------------- GUI Initialisation --------}

startup ('Setting menues'' and frames'' sizes');
setup_iBIOS_menues;

textMode (co80);
textColor (darkGray);
textBackground(black);
turnCursor(false);
window (1,1,80,25);
clrScr;

viewerYmax := 25;
initMenuGrafix;
updateDisplay;

with fadeVar do begin
 color.red    := 0; dir[1] := NoFade;
 color.green  := 0; dir[2] := NoFade;
 color.blue   := 0; dir[3] := Up;
 color.number := 1;
 delayTime    := 10;
end;

{------------------------------------------- Main Routines --------}
repeat
 fadeWait;
 c := readkey;
 case c of
  {#10: screenSaveExecute;}
  #27: ExitToDos;
  #13,#32:
       begin
        with mf.menuf[mf.currentF].entry[mf.menuf[mf.currentF].selected] do
         if isExe then proc;
         updateDisplay;
       end;
  #00:
   begin
    c := readkey;
    case c of
     #75: with mf do
           if currentF > 1 then begin
            dec(currentF);
            updateDisplay;
           end;
     #77: with mf.menuf[mf.currentF] do
           if entry[selected].sub <> nil
           then begin
            inc(mf.currentF);
            updateDisplay;
           end;
     #72:
      with mf.menuf[mf.currentF] do begin
       if selected > 1 then begin
        dec(selected);
        mf.menuf[mf.currentF+1].selected := 1;
       end else
        while entry[selected+1].name <> '' do inc(selected);
       mf.menuf[mf.currentF+1].selected := 1;
       if entry[selected].sub = nil
       then mf.menuf[mf.currentF+1].entry := empty
       else mf.menuf[mf.currentF+1].entry := entry[selected].sub^;
       updateDisplay;
      end;
     #80:
      with mf.menuf[mf.currentF] do begin
       if entry[selected+1].name <> ''
       then inc(selected)
       else while selected > 1 do dec(selected);
       mf.menuf[mf.currentF+1].selected := 1;
       if entry[selected].sub = nil
       then mf.menuf[mf.currentF+1].entry := empty
       else mf.menuf[mf.currentF+1].entry := entry[selected].sub^;
       updateDisplay;
      end;
    end;
   end;
 end;
until false;
end.

