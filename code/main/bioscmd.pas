procedure help (topic : openstring);
begin
 topic := upcase_s(topic);
 if topic = '' then begin
  writeLn (ibiosfull,' Command Line Interface');
  writeLn ('Usages:');
  writeLn ('iBIOS WipeCMOS');
  writeLn ('iBIOS SaveCMOS A:\CMOS.BIO');
  writeLn ('iBIOS RestoreCMOS A:\CMOS.BIO');
  writeLn ('iBIOS Reboot');
  writeLn ('iBIOS Shutdown');
  writeLn ('iBIOS DumpBiosROM A:\DUMP.ROM');
  writeLn ('iBIOS CrackAmi');
  writeLn ('iBIOS CrackAward');
  writeLn ('iBIOS CrackDtk');
  writeLn ('iBIOS CrackIBM');
  writeLn ('iBIOS CrackPhoenix');
  writeLn ('iBIOS CrackStrings');
  writeLn ('iBIOS Sandbox A:\SANDBOX.BIO Crack...');
 end else
 if topic = 'RESTORE1V' then begin
  writeLn ('Sorry, RestoreCMOS requires a filename!');
 end else
 if topic = 'ROM1V' then begin
  writeLn ('Sorry, DumpBiosROM requires a filename!');
 if topic = 'SANDBOX1V' then begin
  writeLn ('Sorry, Sandbox requires a filename!');
 end else
  writeLn ('An internal error in the help database was found!');
 end;
 halt;
end;

procedure handleCommandPrompt;
var
 b         : byte;
 w         : word;
 s         : string80;
 fn        : string80;
 f         : file;
 parami    : byte;
begin
 write_ViaDOS;
 parami := 0;
 while (parami < paramCount) do begin
  inc(parami);
  s := upcase_s (paramStr(parami));
  if s = 'CRACKSTRINGS' then begin
   cmosstringsNoColors;
  end else
  if s = 'CRACKAMI' then begin
   writeLn ('AMI #1: ', amicrack(1));
   writeLn ('AMI #2: ', amicrack(2));
   writeLn ('AMI #3: ', amicrack(3));
  end else
  if s = 'CRACKIBM' then begin
   writeLn ('IBM SurePath PS/2: ', ibm1decode);
  end else
  if s = 'CRACKAWARD' then begin
   writeLn ('Award System:           ', award450_fastAttack(1));
   writeLn ('Award User #1:          ', award450_fastAttack(2));
   writeLn ('Award User #2:          ', award450_fastAttack(5));
   writeLn ('Award 4.50 Master:      ', award450_fastAttack(3));
   writeLn ('Award 4.51/4.60 Master: ', AwardM460_Decode($F000,$EC60,8));
  end else
  if s = 'CRACKDTK' then begin
   writeLn ('DTK Power-On: ', getDTK427pwd($38,4));
   writeLn ('DTK Setup:    ', getDTK427pwd($3B,6));
  end else
  if s = 'CRACKPHOENIX' then begin
   writeLn ('Phoenix Power-On: ', scanDEcode($48,7));
   writeLn ('Phoenix Setup:    ', scanDEcode($50,7));
   writeLn ('Phoenix Ambra:    ', scanDEcode($38,7));
  end else
  if s = 'REBOOT' then bootcold else
  if s = 'SHUTDOWN' then SHUTDOWN else

  if s = 'WIPECMOS' then begin
   cmosByteWrite($2E,0);
   cmosByteWrite($2F,0);
  end else

  if s = 'SAVECMOS' then begin
   for b := 0 to 127 do cmosArray[b] := cmosByteRead(b);
   if parami >= paramcount then help('SAVECMOS1V');
   inc(parami);
   fn := paramStr(parami);
   b := 0;
   while fileExists (fn) do begin
    inc(b);
    fn := removeExtension(fn)+'.'+abc(b,3);
   end;
   assign (f, fn);
   rewrite (f, 1);
   blockwrite (f, cmosArray, sizeOf(cmosArray));
   close (f);
  end else

  if s = 'RESTORECMOS' then begin
   if parami >= paramCount then help ('RESTORE1V');
   inc(parami);
   assign (f, paramStr(parami));
   reset (f, 1);
   blockread (f, cmosArray, sizeOf(cmosArray));
   close (f);
   for b := 0 to 127 do cmosByteWrite(b, cmosArray[b]);
  end else

  if s = 'SANDBOX' then begin
   if parami >= paramCount then help ('SANDBOX1V');
   inc(parami);
   assign (f, paramStr(parami));
   reset (f, 1);
   blockread (f, cmosSandboxArray, sizeOf(cmosSandboxArray));
   close (f);
   cmosSandboxUse := True;
  end else

  if s = 'DUMPBIOSROM' then begin
   if parami >= paramCount
   then begin
     help ('ROM1V');
     halt;
   end;
   inc(parami);
   assign (f, paramStr(parami));
   rewrite (f, 1);
   for w := 0 to $FFFF do begin
    blockwrite (f, mem[$F000:w], 1);
   end;
   close (f);
  end else
   help('');
 end;
 if paramcount <> 0 then halt;
end;
