procedure displayMCGAlogo;
type mcgaCache = array [0..320*200-1] of byte;
var
 f   : file;
 fm  : word;
 b   : byte;
 pal : array [0..255,0..2] of byte;
 p,p2: ^mcgaCache;
begin
 asm
  mov ax,13h
  int 10h
 end;

 fm := filemode;
 filemode := 0; {read only}

 assign(f, bioslogo);
 reset (f, 1);
 seek  (f, 10);
 blockread (f, pal, sizeOf(pal));
 for b := 0 to 255 do setVGA_Pal256 (b,pal[b,0],pal[b,1],pal[b,2]);
 new(p);
 blockread (f, p^, 320*200);
 p2 := ptr($a000,0);
 move (p^, p2^, 320*200);
 dispose(p);
 if readkey = #0 then readkey;
 asm
  mov ax,13h
  int 10h
 end;

 filemode := fm;
end;

procedure nicefadeintro;
var
 c,co : vga256;
 b    : byte;
 w    : word;
begin
 turnCursor(off);
 textcolor(7);
 textbackground(0);
 clrScr;

 with co do begin
  SetVGA_Pal256 (number,red,green,blue);
  number := 0;
  GetVGA_Pal256 (number,red,green,blue);
 end;
 c := co;
 for b := 63 downto 0 do begin
  with c do begin
   red   := b;
   green := b;
   blue  := b;
   waitRetrace;
   setVGA_Pal256 (number,Red,Green,Blue);
  end;
  delay(10);
 end;
end;
