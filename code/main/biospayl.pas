
procedure fadeWaitSS;
const
 sTime = 30; { how much time shall pass before we start timeslicing }
 sScrS = 90; { how much time shall pass before we start screensaver }
var
 Hour, Minute, Second, Sec100: Word;
 ssec, scnt : word;
begin
 ssec := 0;
 scnt := 0;
 screenSaverDone := false;

 while (not keypressed) and (not screenSaverDone) do begin
  fade (fadeVar);
  getTime(Hour, Minute, Second, Sec100);
  if ssec <> second then begin
   inc(scnt);
   ssec := second;
  end;
  if scnt >= sTime then timeslice;
  if scnt >= sScrS then screenSaveExecute;
 end;
end;

procedure fadeWait;
begin
 while not keypressed do fadeWaitSS;
end;

procedure systeminfo;
const f : frame =(
x1: 2;
y1: 6;
x2: 19;
y2: 9);
begin
 makeframe2(f,chStd);
 with f do window (x1,y1,x2,y2+1);
 textColor (lightGray);
 write ('System:  ');
 case test8086 of
  0 : writeLn ('8088');
  1 : writeLn ('80286');
  2 : writeLn ('80386');
  3 : writeLn ('80386');
  4 : writeLn ('80486');
  5 : writeLn ('Pentium');
  6 : writeLn ('PPro');
  7 : writeLn ('Pentium2');
 end;
 writeLn ('DataBus: ', bustype);
 writeLn ('MS-DOS:  ', lo(dosVersion),'.',hi(dosVersion));
 case task.os of
  1 : write ('Windows:');
  2 : write ('OS/2    ');
  3 : write ('DesqView');
  4 : write ('TopView ');
 end;
 case task.os of
  1..4: write (' ',hi(task.Version), '.', lo(task.Version));
 end;
 window(1,1,80,25);
end;

procedure updateDisplay;
var b : byte;
begin
 for b := 1 to noFields do begin
  case fieldsUsage[b] of
   f_menu: displayField(b);
   f_text:
     begin
      with mf.menuf[b] do
       with frm do
        window (x1,y1,x2,y2);
      textBackground(black);
      textColor (mf.cNotF);
      clrScr;
      write (mf.menuF[mf.currentF].entry[mf.menuF[mf.currentF].selected].txt);
      window (1,1,80,25);
     end;
  end;
 end;
end;


procedure InitMenuGrafix;
var b : byte;
begin
 clrScr;
 programBAR ( 1,white, Yellow, iBiosFull, chStd, no);
 programBAR (22,white, Yellow, website+' -{}- bluefish@swipnet.se', chStd, no);
 systemInfo;
 for b := 1 to noFields do
  if fieldsUsage[b] <> f_sleep then makeFrame2 (mf.menuF[b].frm,chStd);
{
 makeFrame2 (mf.menuF[1].frm,chStd);
 makeFrame2 (mf.menuF[2].frm,chStd);
 makeFrame2 (mf.menuF[4].frm,chStd);
 }
 with mf.menuf[mf.currentF] do
  if entry[selected].sub = nil
  then mf.menuf[mf.currentF+1].entry := empty
  else mf.menuf[mf.currentF+1].entry := entry[selected].sub^;
end;


procedure paintDanger (Danger : boolean);
const f : frame =(
x1: 2;
y1: 0;
x2: 19;
y2: 0);
begin
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  if Danger then begin
   makeframe2(f,chStd);
   gotoxy (1,y1+1);
   textColor (lightRed+blink);
   centerWrite (x1+(x2-x1) div 2, ' WARNING: DANGER!');
  end else begin
   window (x1-1,y1-1,x2+1,y2+1);
   clrScr;
   window (1,1,80,viewerYmax);
  end;
 end;
end;

function AreYouSure (msg : string40) : boolean;
var
 s : string;
 c : char;
 f : frame;
begin
 s := 'ARE YOU SURE YOU WANT TO '+msg+'? (Y/N)';
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  x1 := mf.menuf[1].frm.x1;
  x2 := x1+2+length(s);
  makeframe2(f,chStd);
  textColor(lightRed);
  gotoxy (x1+1,y1+1);
  write  (s);
 end;
 viewerBeep;
 repeat
  fadeWait;
  c := upcase(readkey);
  if c = #0 then readkey;
 until (c = 'Y') or (c = 'N') or (c = #27);
 if c = 'Y'
 then AreYouSure := Yes
 else AreYouSure := No;
 with f do begin
  window (x1-1,y1-1,x2+1,y2+1);
  clrScr;
 end;
 window (1,1,80,25);
end;

procedure reboot;
begin
 if areYouSure ('REBOOT') then BootCold;
end;

procedure biosAttack1;
begin
 paintDanger(on);
 if areYouSure ('WIPE BIOS') then begin
  cmosByteWrite($2E,0);
  cmosByteWrite($2F,0);
 end;
 paintDanger(off);
end;

procedure biosAttack2;
begin
 paintDanger(on);
 if areYouSure ('WIPE BIOS') then begin
  cmosByteWrite($2E,random(256));
  cmosByteWrite($2F,random(256));
 end;
 paintDanger(off);
end;

procedure biosAttack6;
begin
 paintDanger(on);
 if areYouSure ('WIPE BIOS') then begin
  cmosByteWrite($7A,0);
  cmosByteWrite($7B,0);
 end;
 paintDanger(off);
end;

procedure biosAttack7;
begin
 paintDanger(on);
 if areYouSure ('WIPE BIOS') then begin
  cmosByteWrite($7D,0);
  cmosByteWrite($7E,0);
 end;
 paintDanger(off);
end;

procedure biosAttack3;
var b : byte;
begin
 paintDanger(on);
 if areYouSure ('WIPE BIOS') then
  for b := 0 to 127 do cmosByteWrite(b,random(256));
 paintDanger(off);
end;

procedure biosAttack4;
var w : word;
begin
 paintDanger(on);
 if areYouSure ('LAUNCH A PORT ATTACK') then
  for w := 0 to 65535 do
   PORT[random(65535)] := random(256);
 paintDanger(off);
end;

procedure biosAttack5;
var w : word;
begin
 paintDanger(on);
 if areYouSure ('LAUNCH A PORT ATTACK') then
  for w := 0 to 65535 do
   PORT[w] := random(256);
 paintDanger(off);
end;

procedure cmoseditor;
begin
  simpleCmosEditor;
  turnCursor(Off);
  initMenuGrafix;
end;
{$I BIOSSRC.PAS}

procedure DisplayBiosSourceAsm;
begin
 viewerByteArray2Array (BIOSASM,sizeOf(BIOSASM));
 viewerInfF := '!BIOSASM.ASM';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Assembler source: BIOS Wipe!';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

procedure DisplayBiosSourcePas;
begin
 viewerByteArray2Array (BIOSPAS,sizeOf(BIOSPAS));
 viewerInfF := '!BIOSPAS.PAS';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Pascal source: BIOS Wipe!';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

procedure DisplayBiosSourceDebug;
begin
 viewerByteArray2Array (BIOSDEBUG,sizeOf(BIOSDEBUG));
 viewerInfF := '!BIOSDEB.DBG';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Pascal source: BIOS Wipe!';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

procedure DisplayBiosSourceTxt;
begin
 viewerByteArray2Array (BIOSTEXT,sizeOf(BIOSTEXT));
 viewerInfF := '!BIOSTED.TXT';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Text source: BIOS Wipe!';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;


procedure DisplayBiosDoc;
begin
 viewerPointer2array (addr(BIODX),BIODXLEN);
 viewerInfF := '!BIOS.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := '!BIOS Documentation';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

{procedure DisplayBiosDocFaq;
begin
 viewerPointer2array (addr(BIODXFAQ),BIODXFAQLEN);
 viewerInfF := '!BIOSFAQ.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := '!BIOS Frequently Asked Questions';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;}

procedure DisplayBiosDocPasswords;
begin
 viewerPointer2array (addr(BIODXDEF),BIODXDEFLEN);
 viewerInfF := 'BIOSDEF.LST';
 viewerInfA := 'Nathan Einwechter';
 viewerInfS := 'Default/Master Passwords';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;


procedure DisplayBiosHowTo;
begin
 viewerPointer2array (addr(BIODXHOW),BIODXHOWLEN);
 viewerInfF := 'HOWTO.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := 'How To Use !BIOS';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

procedure DisplayBiosDocEleventh;
begin
 viewerPointer2array (addr(BIODX11A),BIODX11ALEN);
 viewerInfF := 'ELEVENTH.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Elventh Alliance & hacking';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;

{procedure DisplayBiosDocRipped;
begin
 viewerPointer2array (addr(BIODXRIP),BIODXRIPLEN);
 viewerInfF := 'RIPPED.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Borrowed stuff...';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;}
{procedure DisplayBiosDocPaying;
begin
 viewerPointer2array (addr(BIODXPAY),BIODXPAYLEN);
 viewerInfF := 'PAYING.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Shareware - Not!';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;}
procedure DisplayBiosDocTips;
begin
 viewerPointer2array (addr(BIODXTIP),BIODXTIPLEN);
 viewerInfF := 'TIPS.DOC';
 viewerInfA := 'Bluefish';
 viewerInfS := 'Various tips';
 viewerYmax := 25;
 viewerCntColor  := darkGray;
 viewerTextColor := cyan;
 viewerInit (chstd);
 viewerScrUpdate;
 viewerMainRoutine;
 InitMenuGrafix;
end;


procedure DisplayTestInfo2;
begin
 DisplayTestInfo;
 InitMenuGrafix;
end;


procedure AMI (s : string40);
var
 c : char;
 f : frame;
begin
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  x1 := mf.menuf[1].frm.x1;
  x2 := x1+2+length(s);
  makeframe2(f,chStd);
  textColor(lightRed);
  gotoxy (x1+1,y1+1);
  write  (s);
 end;
 fadeWait;
 while keypressed do readkey;
 with f do begin
  window (x1-1,y1-1,x2+1,y2+1);
  clrScr;
 end;
 window (1,1,80,25);
end;



procedure AMI_all;
begin
 {AMI('Oldies: '+amicrack(1));
 AMI('Winbios: '+amicrack(2));
 AMI('New: '+amicrack(3));}
 AMI('1: '+amicrack(1)+'  2: '+amicrack(2)+'  3: '+amicrack(3));
end;
procedure IBM1;
var s : string20;
begin
 s := ibm1decode;
 AMI(s);
end;


procedure award450;
begin
 award450brute;
 InitMenuGrafix;
end;

procedure DTKpowersetup;
begin
 ami('POWER: '+getDTK427pwd($38,4)+' SETUP: '+getDTK427pwd($3B,6));
end;

procedure phoenix404;
begin
 ami('USER: '+scanDEcode($48,7) + ' SETUP: '+scanDEcode($50,7));
end;

procedure phoenixAmbra;
begin
 ami(scanDEcode($38,7));
end;


procedure shutdownProc;
begin
 textMode (lsm);
 shutdown;
 halt;
end;

var
 c1org   : vga256;
 sptr    : ptrSSD;
procedure ExitToDos;
var
 b : byte;
begin
 textMode (lsm);
 screenSaverExit(sptr);
 turnCursor(On);
 b := textAttr;
 textColor(yellow);
 textBackground(black);
 writeLn(
  'Eleventh Alliance would like to thank you for using !BIOS.'+rad+
  'To get a new version or to join the development team, visit our website:'+rad+website+rad);
 writeLn(
  'If !BIOS doesn''t do the trick for you, try CmosPwd by Christophe Grenier:'+rad+
  'http://www.esiea.fr/public_html/Christophe.GRENIER/');
 textAttr := b;
 writeLn;
 if dateYYYYMM > alphaLastDate then
    writeLn ('This version seems old. How about updating?');
 halt(0);
end;

function getFileName (x,y,max : byte; var fileName : string ) : boolean;
var s : string; c : char; lc, b : byte; bo : boolean;
begin
        getFileName := No;
        viewerBeep;
        s := fileName;
        turnCursor(on);
        repeat
         gotoXY (x,y);
         write (s+' ');
         gotoxy (whereX-1,whereY);
         fadeWait;
         c := upcase(readkey);
         case c of
          '<', '>', '+', ',', '/','*','?': ;
          #0: readkey;

          #8:  if length(s) > 0 then s[0] := char(length(s)-1);
          ':': if length(s) = 1 then s := s+c;
          '\': if s[length(s)] <> '\' then s := s+c;
          '.': if s[length(s)] <> '.' then s := s+c;
          #33..#128:
             begin
              if length(s) < max then begin
               lc := 0;
               for b := 1 to length(s) do if (s[b] = '.') or (s[b] = ':') or (s[b] = '\') then lc := b;
               bo := true;
               if (length(s)-lc = 3) then if (s[lc] = '.') then bo := false;
               if (length(s)-lc = 8) then if (s[lc] = ':') then bo := false;
               if (length(s)-lc = 8) then if (s[lc] = '\') then bo := false;
               if bo then s := s+c;
              end;
             end;
          #13: begin
            turnCursor(Off);
            if length(s) > 3
            then begin
             fileName := s;
             getFileName := Yes;
            end;
            c := #27;
          end;
         end;
        until c = #27;
        turnCursor(Off);
end;


procedure errorBeepWait;
begin
 viewerBeep;
 fadeWait;
 readkey;
end;

function defaultCMOSfile : string20;
begin
 defaultCMOSfile := 'A:\CMOS'+hexbyte(cmosByteRead($2e))+hexbyte(cmosByteRead($2f))+'.BIO';
end;


procedure CMOSLoad;
var
 b   : byte;
 f   : frame;
 s   : string;
 out : file;
 IOR : integer;
begin
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  x1 := mf.menuf[1].frm.x1;
  x2 := 78;
  makeframe2(f,chStd);
  gotoXY (x1,y1);
  textColor (white);
  write ('Enter file/device where from restore CMOS settings: ');
  s := defaultCMOSfile;
  if getFileName (x1,y1+1,x2-x1-2,s) then begin
   if fileExists (s) then begin
{$IFNDEF NDM} {$I-} {$ENDIF}
    assign (out,s);
    reset  (out,1);
    IOR := IOResult;
    if fileSize(out) = sizeOf(cmosArray) then begin
     if IOR = 0 then blockRead (out,cmosArray,sizeOf(cmosArray));
     if IOR = 0 then IOR := IOResult;
     if IOR = 0 then close (out);
     if IOR = 0 then IOR := IOResult;
{$IFNDEF NDM} {$I+} {$ENDIF}
     if IOR <> 0 then begin
      window (x1,y1,x2,y2);
      clrScr;
      window (1,1,80,25);
      gotoXY (x1,y1+1);
      centerWrite (x1+(x2-x1)div 2,'Error '+abc(IOR,1)+': '+getErrorMsg(IOR));
      errorBeepWait;
     end else for b := 0 to 127 do cmosByteWrite(b,cmosArray[b]);
    end else begin
     close (out);
     window (x1,y1,x2,y2);
     clrScr;
     window (1,1,80,25);
     gotoXY (x1,y1+1);
     centerWrite (x1+(x2-x1)div 2,'File is not a CMOS backup!');
     errorBeepWait;
    end;
   end else begin
    window (x1,y1,x2,y2);
    clrScr;
    window (1,1,80,25);
    gotoXY (x1,y1+1);
    centerWrite (x1+(x2-x1)div 2,'File does not exist!');
    errorBeepWait;
   end;
  end;
  window (x1-1,y1-1,x2+1,y2+1);
  clrScr;
  window (1,1,80,25);
 end;
end;


procedure BIOSSave;
var
 b   : byte;
 f   : frame;
 s   : string;
 out : file;
 IOR : integer;
begin
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  x1 := mf.menuf[1].frm.x1;
  x2 := 78;
  makeframe2(f,chStd);
  gotoXY (x1,y1);
  textColor (white);
  write ('Enter file/device where dump ROM chip: ');
  s := 'A:\BIOSDUMP.ROM';
  if getFileName (x1,y1+1,x2-x1-2,s) then romdump(s);
  window (x1-1,y1-1,x2+1,y2+1);
  clrScr;
  window (1,1,80,25);
 end;
end;



procedure CMOSSave;
var
 b   : byte;
 f   : frame;
 s   : string;
 out : file;
 IOR : integer;
begin
 for b := 0 to 127 do cmosArray[b] := cmosByteRead(b);
 with f do begin
  y1 := mf.menuf[1].frm.y2+3;
  y2 := y1+2;
  x1 := mf.menuf[1].frm.x1;
  x2 := 78;
  makeframe2(f,chStd);
  gotoXY (x1,y1);
  textColor (white);
  write ('Enter file/device where to back up CMOS settings: ');
  s := defaultCMOSfile;
  if getFileName (x1,y1+1,x2-x1-2,s) then begin
   {$I-}
   assign     (out,s);
   rewrite    (out,1);
   IOR := IOResult;
   if IOR = 0 then begin
    blockwrite (out,cmosArray,sizeOf(cmosArray));
    IOR := IOResult;
   end;
   if IOR = 0 then begin
    close (out);
    IOR := IOResult;
   end;
   {$I+}
   if IOR <> 0 then begin
    window (x1,y1,x2,y2);
    clrScr;
    window (1,1,80,25);
    gotoXY (x1,y1+1);
    centerWrite (x1+(x2-x1)div 2,'Error '+abc(IOR,1)+': '+getErrorMsg(IOR));
    errorBeepWait;
   end;
  end;
  window (x1-1,y1-1,x2+1,y2+1);
  clrScr;
  window (1,1,80,25);
 end;
end;

procedure cmosstrings;
var i : integer; c : char;
begin
clrscr;
textcolor(white);
writeLn('Scancodes: ');
textcolor(lightgray);
for i := 0 to 127 do begin
 c := scan2ascii(chr(cmosByteRead(i)));
 if ord(c) >= 32 then write(c) else write('.');
 if i = 63 then writeLn;
end;
textcolor(white);
writeLn(rad+'Ascii strings:');
textcolor(lightgray);
for i := 0 to 127 do begin
 c := chr(cmosByteRead(i));
 if ord(c) >= 32 then write(c) else write('.');
 if i = 63 then writeLn;
end;
if readkey = #0 then readkey;
InitMenuGrafix;
end;

procedure cmosstringsNoColors;
var i : integer; c : char;
begin
writeLn('Scancodes: ');
for i := 0 to 127 do begin
 c := scan2ascii(chr(cmosByteRead(i)));
 if ord(c) >= 32 then write(c) else write('.');
 if i = 63 then writeLn;
end;
writeLn(rad+rad+'Ascii strings:');
for i := 0 to 127 do begin
 c := chr(cmosByteRead(i));
 if ord(c) >= 32 then write(c) else write('.');
 if i = 63 then writeLn;
end;
end;

  procedure cmosSandboxMenu;
  var
   s : string;
   f : file;
   i : integer;
  begin
    while true do begin
      textColor(lightgray);
      clrScr;
      writeLn ('[1] Sandbox in use: ', cmosSandboxUse);
      writeLn ('[2] Load true cmos into sandbox');
      writeLn ('[*] Press any other key to exit.');
      writeLn;
      writeLn ('Once Sandbox is enabled, use the CMOS load/backup features');
      writeLn ('to load/save the sandbox.');
      writeLn;
      textColor(lightgreen);
      writeLn ('Sandbox is used for testing new algorithms at development level,');
      writeLn ('and/or run the password crackers or the editor from remote workstation.');
      writeLn ('While sandbox is in use, no real read/writes to CMOS are done,');
      writeLn ('!bios *appears* to work as ordinary but it is really working with');
      writeLn ('sandbox instead of the cmos.');
      writeLn;
      writeLn ('If you don''t understand it, just forget about it.');
      writeLn;
      textColor(lightgray);
      writeLn;
      case readkey of
       #00: readkey;
       '1': begin
              if cmosSandboxUse
              then cmosSandboxUse := false
              else cmosSandboxUse := true;

              writeLn ('OK - Sandbox on/off has been toggled.');
              bell(100,200);
              delay(300);
            end;
        '2': begin
               cmosSandboxUse := false;
               for i := 0 to 127 do cmosSandboxArray[i] := cmosByteRead(i);
               cmosSandboxUse := true;

               writeLn;
               writeLn ('OK - CMOS loaded into the sandbox!');
               bell(100,200);
               delay(300);
             end;
        else
        break;
      end;
    end;
  end;


procedure cmosSandboxMenuRun;
begin
cmosSandboxMenu;
InitMenuGrafix;
end;

var biosWipes, sources, cmosbak, docs, decoders,various,advanced : menuEntryArray;
procedure Setup_iBIOS_menues;
begin

 fieldsUsage[1] := f_menu;
 fieldsUsage[2] := f_menu;
 fieldsUsage[3] := f_sleep;
 fieldsUsage[4] := f_text;
 fieldsUsage[5] := f_sleep;

 with mf do begin
  cNotF := lightGray;
  cNotB := black;
  cSelF := white;
  cSelB := 1;
  currentF:= 1;
  with menuF[1] do begin
   frm.x1:= 22;  frm.y1 := 6;
   frm.x2:= 36;  frm.y2 := 15;
   selected:= 1;
   with entry[1] do begin
    name  := 'Docs';
    txt   := iBios+rad+
             '(c) Eleventh Alliance,'+rad+
             '    Bluefish & ibiosdev'+rad+rad+
             ' '+#24+rad+
             #27+' '+#26+' Keyboard contols!'+rad+
             ' '+#25+'  Don''t forget Enter!';
    isExe := No;
    sub   := addr(docs);
   end;
   with entry[2] do begin
    name  := 'Crackers';
    txt   := 'Decrypt Passwords!'+rad+rad+
             'Try mastercodes too!'+rad+
             '  589589'+rad+'  AMI'+rad+'  AWARD_SW'+rad+'  AWARD SW'+rad+'  AWARD'+rad+'  SZYX';
    isExe := No;
    sub   := addr(decoders);
   end;
   with entry[3] do begin
    name  := 'Backup';
    txt   := 'CMOS Backup/Restore';
    isExe := No;
    sub   := addr(cmosbak);
   end;
   with entry[4] do begin
    name  := 'Blasters';
    txt   := 'Launch a BIOS attack!'+rad+rad+
             'WARNING:'+rad+rad+
             'a few bioses may not be'+rad+
             'able to boot after a'+rad+
             'blast. Think twice!';
    isExe := No;
    sub   := addr(biosWipes);
   end;
   with entry[5] do begin
    name  := 'Advanced';
    txt   := 'For techs only..!';
    isExe := No;
    proc  := Nil;
    sub   := addr(advanced);
   end;
   with entry[6] do begin
    name  := 'Source';
    txt   := 'How-To-Do a blaster!';
    isExe := No;
    sub   := addr(sources);
   end;
  with entry[7] do begin
   name  := 'Quit';
   txt   := 'MOV AH,4C'+rad+'MOV AL,ErrLvl'+rad+'INT 21';
   isExe := Yes;
   Proc  := ExitToDos;
   sub   := addr(various);
  end;
  with entry[8] do begin
   name  := 'Screen Saver';
   txt   := 'Can''t you wait for it?';
   isExe := Yes;
   proc  := ScreenSaveExecute;
   sub   := Nil;
  end;
  with entry[9] do begin
   name  := 'Junkinfo';
   txt   := 'Various info from units';
   isExe := Yes;
   Proc  := displayTestInfo2;
   sub   := Nil;
  end;
  entry[10].name := '';
 end;
 with menuF[2] do begin
  frm.x1:= 39;  frm.y1 := 6;
  frm.x2:= 51;  frm.y2 := 15;
  selected:= 1;
 end;
 with menuF[4] do begin
  frm.x1:= 54;  frm.y1 := 6;
  frm.x2:= 78;  frm.y2 := 15;
 end;
end;
  with various[1] do begin
   name  := 'Quit';
   txt   := 'MOV AH,4C'+rad+'MOV AL,ErrLvl'+rad+'INT 21';
   isExe := Yes;
   Proc  := ExitToDos;
   sub   := Nil;
  end;
  with various[2] do begin
   name  := 'Reboot';
   txt   := 'JMP FFFF:0000';
   isExe := Yes;
   proc  := reboot;
   sub   := Nil;
  end;
  with various[3] do begin
   name  := 'Shutdown';
   txt   := 'Turns power off'+rad+'works with some computers';
   isExe := Yes;
   proc  := ShutdownProc;
   sub   := Nil;
  end;
  various[4].name := '';


   with advanced[1] do begin
    name  := 'Edit';
    txt   := 'A crude CMOS editor'+rad+rad+'a useless hack ;-)';
    isExe := Yes;
    proc  := CmosEditor;
    sub   := Nil;
   end;
   with advanced[2] do begin
    name  := 'Sandbox';
    txt   := 'Read/write to memory'+rad+'instead of CMOS.';
    isExe := Yes;
    proc  := cmosSandboxMenuRun;
    sub   := Nil;
   end;
   with advanced[3] do begin
    name  := 'Bios Dump';
    txt   := 'Dump ROM chip to file';
    isExe := Yes;
    Proc  := BIOSSave;
    sub   := Nil;
   end;
   advanced[4].name := '';

  with decoders[1] do begin
   name  := 'AMI';
   txt   := 'American Megatrends Inc'+rad+'(three crackers)';
   isExe := Yes;
   proc  := AMI_all;
   sub   := Nil;
  end;
  with decoders[2] do begin
   name  := 'Award';
   txt   := 'Award 4.50 / 4.51 / 4.60'+rad+'(and others?)';
   isExe := Yes;
   proc  := award450;
   sub   := Nil;
  end;
  with decoders[3] do begin
   name  := 'DTK';
   txt   := 'DTK DSN Notebook'+rad+'BIOS 4.27, Setup 3.0';
   isExe := Yes;
   proc  := DTKpowersetup;
   sub   := Nil;
  end;
  with decoders[4] do begin
   name  := 'IBM';
   txt   := 'IBM SurePath BGOSV1E'+rad+'(and others)'+rad+rad+'Used in some IBM PS/2s';
   isExe := Yes;
   proc  := ibm1;
   sub   := Nil;
  end;
  with decoders[5] do begin
   name  := 'Phoenix 1';
   txt   := 'Phoenix BIOS 4.04'+rad+'(and others)';
   isExe := Yes;
   proc  := phoenix404;
   sub   := Nil;
  end;
  with decoders[6] do begin
   name  := 'Phoenix 2';
   txt   := 'Phoenix Ambra'+rad+'(and others)';
   isExe := Yes;
   proc  := phoenixAmbra;
   sub   := Nil;
  end;
  with decoders[7] do begin
   name  := 'Strings';
   txt   := 'Show entire CMOS as '+rad+
            'ASCII and scancode'+rad+
            'strings...'+rad+rad+
            '(cracks several simple'+rad+
            'bioses ;-)';
   isExe := Yes;
   proc  := cmosstrings;
   sub   := Nil;
  end;
  decoders[8].name := '';


  with cmosbak[1] do begin
   name  := 'BackUp';
   txt   := 'BackUp all CMOS/BIOS'+rad+'settings';
   isExe := Yes;
   proc  := cmosSave;
   sub   := Nil;
  end;
  with cmosbak[2] do begin
   name  := 'Restore';
   txt   := 'Restore all CMOS/BIOS'+rad+'settings';
   isExe := Yes;
   Proc  := CMOSLoad;
   sub   := Nil;
  end;
  cmosbak[3].name := '';
  with biosWipes[1] do begin
   name  := 'Attack 1';
   txt   := 'CMOS:2E = 0'+rad+'CMOS:2F = 0'+rad+rad+'Standard Attack';
   isExe := Yes;
   Proc  := biosAttack1;
   sub   := Nil;
  end;
  with biosWipes[2] do begin
   name  := 'Attack 2';
   txt   := 'CMOS:2E = RND'+rad+'CMOS:2F = RND'+rad+rad+'Standard Attack'+rad+'(slightly modified)';
   isExe := Yes;
   Proc  := biosAttack2;
   sub   := Nil;
  end;
  with biosWipes[3] do begin
   name  := 'Attack 3';
   txt   := 'CMOS:ALL = RND'+rad+rad+rad+'Slightly dangerous!';
   isExe := Yes;
   Proc  := biosAttack3;
   sub   := Nil;
  end;
  with biosWipes[4] do begin
   name  := 'Attack 4';
   txt   := 'PORT[RND] = RND'+rad+rad+rad+'Slightly dangerous!'+rad+rad+rad+'Does usually not work';
   isExe := Yes;
   Proc  := biosAttack4;
   sub   := Nil;
  end;
  with biosWipes[5] do begin
   name  := 'Attack 5';
   txt   := 'PORT[ALL] = RND'+rad+rad+rad+'Slightly dangerous!'+rad+rad+rad+'Does usually not work';
   isExe := Yes;
   Proc  := biosAttack5;
   sub   := Nil;
  end;
  with biosWipes[6] do begin
   name  := 'Attack 6';
   txt   := 'CMOS:7A = 0'+rad+'CMOS:7B = 0'+rad+rad+'Extended Attack'+rad+rad+'(attacks award''s'+rad+'extended checksum)';
   isExe := Yes;
   Proc  := biosAttack6;
   sub   := Nil;
  end;
  with biosWipes[7] do begin
   name  := 'Attack 7';
   txt   := 'CMOS:7D = 0'+rad+'CMOS:7E = 0'+rad+rad+'Extended Attack'+rad+rad+
            '(attacks award''s'+rad+'extended checksum,'+rad+'other versions)';
   isExe := Yes;
   Proc  := biosAttack7;
   sub   := Nil;
  end;
  biosWipes[8].name := '';
  empty[1].name := '';
  with docs[1] do begin
   name  := 'Info';
   txt   := 'Information about'+rad+iBios
             {$IFDEF AlphaTest}
             +rad+rad+rad+rad+'Alpha '+alphaVersion+rad+rad+'TEST VERSION ONLY!'
             {$ENDIF}
             ;
   isExe := Yes;
   Proc  := DisplayBiosDoc;
   sub   := Nil;
  end;
  with docs[2] do begin
   name  := 'How To';
   txt   := 'How To use !BIOS';
   isExe := Yes;
   Proc  := DisplayBiosHowTo;
   sub   := Nil;
  end;
  with docs[3] do begin
   name  := 'Eleventh';
   txt   := 'Information about'+rad+'Eleventh Alliance';
   isExe := Yes;
   Proc  := DisplayBiosDocEleventh;
   sub   := Nil;
  end;
  with docs[4] do begin
   name  := 'Tips';
   txt   := 'Various tips';
   isExe := Yes;
   Proc  := DisplayBiosDocTips;
   sub   := Nil;
  end;
  with docs[5] do begin
   name  := 'Passwords';
   txt   := 'Default/master'+rad+'password list';
   isExe := Yes;
   Proc  := DisplayBiosDocPasswords;
   sub   := Nil;
  end;
  docs[6].name := '';
  {
  with docs[3] do begin
   name  := 'Faq!';
   txt   := 'Frequently'+rad+'Asked'+rad+'Questions';
   isExe := Yes;
   Proc  := DisplayBiosDocFaq;
   sub   := Nil;
  end;
  }
  {
  with docs[6] do begin
   name  := 'Paying';
   txt   := 'Freeware?'+rad+'Am I supposed to pay?';
   isExe := Yes;
   Proc  := DisplayBiosDocPaying;
   sub   := Nil;
  end;
  }

  with sources[1] do begin
   name  := 'Assembly';
   txt   := 'Assembly language'+rad+rad+rad+'i8088 ASM Source'+rad+rad+rad+'Why not get A86.COM ?';
   isExe := Yes;
   Proc  := DisplayBiosSourceAsm;
   sub   := Nil;
  end;
  with sources[2] do begin
   name  := 'Pascal';
   txt   := 'Pascal source';
   isExe := Yes;
   Proc  := DisplayBiosSourcePas;
   sub   := Nil;
  end;
  with sources[3] do begin
   name  := 'Debugger';
   txt   := 'Debug source'+rad+rad+'DEBUG < !BIOSDEB.DBG'+rad+rad
           +'Tip:'+rad+rad+'-o 70 2E'+rad+'-o 71 00';
   isExe := Yes;
   Proc  := DisplayBiosSourceDebug;
   sub   := Nil;
  end;
  with sources[4] do begin
   name  := 'Text';
   txt   := 'Text source'+rad+rad+'Create Anti-Bios with a'+rad+'texteditor or COPY CON!';
   isExe := Yes;
   Proc  := DisplayBiosSourceTxt;
   sub   := Nil;
  end;
  sources[5].name := '';
end;

{$IFDEF SCk}
procedure selfcheck;
var c : char;
begin;
if (proc11aCheckAV(paramStr(0)) = false)
or (proc11aCheck(paramStr(0)) = false) then begin
  writeLn;
  writeLn ('Program may be virus infected! Scan system for viruses!'+rad+rad);
  writeLn ('Esc to quit to DOS'+rad+
           'Space to start program'+rad);
  repeat
   c := readkey;
   case c of
    #00: readkey;
    #27: halt(255);
   end;
  until c = #32;
end;
end;
{$ENDIF}

