const
 screenSaverDone : boolean = false;

type
 vga256 = record
     number,
     red,
     blue,
     green : byte;
  end;
 fadeOption = (Up,Down,NoFade);
 fadeRecord = record
     color : vga256;
     dir   : array[1..3] of FadeOption;
     delayTime : word;
  end;
var
 fadeVar : fadeRecord;


procedure fade (var f : fadeRecord);
begin
with f do begin
 with color do begin
  case dir[1] of
   Up:   if Red   >= 63 then dir[1] := Down else inc(Red  );
   Down: if Red    =  0 then dir[1] := Up   else dec(Red  );
  end;
  case dir[2] of
   Up:   if Green >= 63 then dir[2] := Down else inc(Green);
   Down: if Green  =  0 then dir[2] := Up   else dec(Green);
  end;
  case dir[3] of
   Up:   if Blue  >= 63 then dir[3] := Down else inc(Blue );
   Down: if Blue   =  0 then dir[3] := Up   else dec(Blue );
  end;
  waitRetrace;
  setVGA_Pal256 (number,Red,Green,Blue);
  delay(delayTime);
 end;
end;
end;

type
 ptrSSD = ^screenSaverData;
 screenSaverData = record
  pY,pX,
  maxy,
  tattr : byte;
  memDump : array [0..80*50*2-1] of byte;
end;

procedure dumpTextMem (var p : ptrSSD);
var w : word;
begin
 with p^ do
 for w := 0 to sizeOf(memDump)-1 do
  memDump[w] := mem[$B800:w];
end;

procedure restoreTextMem (var p : ptrSSD);
var w : word;
begin
 with p^ do
 for w := 0 to sizeOf(memDump)-1 do
  mem[$B800:w] := memDump[w];
end;

procedure screenSaverInit (var p : ptrSSD);
begin
 new (p);
 with p^ do begin
  tattr := textAttr;
  pX := whereX;
  pY := whereY;
  window(1,1,80,25);
  window(1,1,80,43);
  window(1,1,80,50);
  gotoxy(1,25);
  gotoxy(1,43);
  gotoxy(1,50);
  maxy := whereY;
  gotoxy(pX,pY);
 end;
 dumpTextMem (p);
end;

procedure screenSaverExit (var p : ptrSSD);
begin;
 restoreTextMem(p);
 with p^ do begin
  window (1,1,80,maxY);
  gotoXY (pX,pY);
  textAttr := tattr;
 end;
 while keypressed do readkey;
 screenSaverDone := true;
 dispose(p);
end;

procedure screenSaver2(maxy : byte);
var
 fadeVar2 : fadeRecord;

procedure ssFadeSub (times : word);
const
 i = 10;
var
 b : byte;
 w : word;
begin
 for w := 1 to (times div i) do begin
  if keypressed then exit;
  timeSlice;
  for b := 1 to i do fade(fadeVar2);
 end;
end;


var
 x,y : byte;
 msg : string60;
 msgBio : boolean;
const
 msgb : byte = 0;
begin
 with fadeVar2 do begin
  color.red    := 0;  dir[1] := Up;
  color.green  := 30; dir[2] := Up;
  color.blue   := 30; dir[3] := Down;
  color.number := 1;
  delayTime    := 10;
 end;
 clrScr;
 msgBio := false;
 repeat
  {msgBio := msgBio xor true;
  if msgBio
  then msg := iBios+' by the Eleventh Alliance & ibiosdev'
  else} begin
   {inc(msgB);}
   case msgB of
    0: msg := iBios+' by the Eleventh Alliance & ibiosdev';
    1: msg := 'Main developer: Bluefish';
    2: msg := 'Thanks goes to: the people on ibiosdev';
    3: msg := 'Christophe Grenier';
    4: msg := 'Juergen Glockner';
    5: msg := 'Mladen Zelic';
    6: msg := 'CaveMan';
    7: msg := 'White Ninja (nice postcard!)';
    8: msg := 'and lots of others!';
    {else begin
     msg := '- 11A - Eleventh Alliance - 11A -';
     msgB := 0;
    end;}
   end;
   msgB := (msgB+1) mod 9;
  end;
  x := random(80-length(msg)-1)+1;
  y := random(maxY)+1;
  textColor(1); gotoXY (x,y); write (msg); ssFadeSub (200);
  textColor(0); gotoXY (x,y); write (msg);
 until keypressed;
end;

procedure screenSaveExecute;
const
 smax = 2;
 slast : byte = smax;
var screenDump : ptrSSD;
begin
 if slast = smax then slast := random(smax);
 while keypressed do readkey;
 screenSaverInit(screenDump);
 case slast of
  0: screenSaver2   (screenDump^.maxy);
  1: screenSaverFlames;
 end;
 screenSaverExit(screenDump);
 turnCursor(off);
 fade (fadeVar);
 inc(slast);
 if slast = smax then slast := 0;
end;



{
procedure screenSaver1;
var
 c : char;
 b : byte;
 w : word;
 fadeVar2 : fadeRecord;
begin
 with fadeVar2 do begin
  color.red    := 0; dir[1] := NoFade;
  color.green  := 0; dir[2] := Up;
  color.blue   := 0; dir[3] := NoFade;
  color.number := 1;
  delayTime    := 15;
 end;
 clrScr;
 textColor(1);
 centerWriteLn(40,'Analyzing CPU Data Flow:');
 repeat
  window (1,3,80,maxY);
  textColor(green);
  while not keypressed do begin
   timeSlice;
   fade(fadeVar2);
   for b := 1 to 26 do
    write(hexbyte(32+random(256-32))+' ');
   fade(fadeVar2);
   writeLn;
   fade(fadeVar2);
  end;
  window (1,1,80,maxY);
  c := readkey;
  if c = #10 then begin
   clrScr;
   textColor(1);
   case random(4) of
    0: centerWriteLn (40,'Analyzing Brainwaves:');
    1: centerWriteLn (40,'Searching for Extra-Terristial Intelligence:');
    2: centerWriteLn (40,'Trashing harddisk:');
    3: centerWriteLn (40,'Detected Overflows:');
   end;
  end;
 until c <> #10;
end;
}


