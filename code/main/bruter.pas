
type bruteforcer = object
 charset : string80;
 workStr : string;
 result  : string20;
 procedure init(n : byte);
 procedure brute;
 procedure bruteR (ws,r,cset : pointer);
end;

procedure bruteforcer.init (n : byte);
var b : byte;
begin
 workstr := '';
 for b := 1 to n do workstr := workstr+#1;
 bruteR (addr(workStr), addr(result), addr(charset));
end;

procedure bruteforcer.brute;
begin
 bruteforceAsm (workStr, #1, charset[0]);
 bruteR (addr(workStr), addr(result), addr(charset));
end;

procedure bruteforcer.bruteR (ws,r,cset : pointer); assembler;
asm
 pusha
 push  ds
 push  es
 cld
 les  di, r
 lds  si, ws
 cli
 mov  word ptr cs:[@sskeepalive],ss
 mov  dx, word ptr ss:[cset]
 mov  ss, word ptr ss:[cset+2]

 lodsb
 stosb
 xor  ch, ch
 xor  ah, ah
 mov  cl, al
 mov  al, cl
 jmp  @l
@sskeepalive:
 dw   0

@l:
 lodsb
 mov  bx, dx
 add  bx, ax
 mov  al, ss:[bx]
 stosb
 loop @l

 mov  ss, word ptr cs:[@sskeepalive]
 sti
 pop  es
 pop  ds
 popa
end;
