
function FileExists(FileName: string)
                                : Boolean;
{ Returns True if file exists; otherwise,
  it returns False. Closes the file if
  it exists. }
var
  f: file;
  b: byte;
begin
 b:= system.filemode;
 system.filemode := 0;
 {$I-}
 Assign(F, FileName);
 Reset(F);
 Close(F);
 {$I+}
 FileExists := (IOResult = 0) and (FileName <> '');
 system.filemode := b;
end;  { FileExists }


function FileExistsSHARE(FileName: string)
                                : Boolean;
{ Returns True if file exists; otherwise,
  it returns False. Closes the file if
  it exists. }
var
  f: file;
  b: byte;
begin
 b:= system.filemode;
 system.filemode := 64;
 {$I-}
 Assign(F, FileName);
 Reset(F);
 Close(F);
 {$I+}
 FileExistsSHARE := (IOResult = 0) and (FileName <> '');
 system.filemode := b;
end;  { FileExists }


function exists (var a_file : text) : boolean;
var temp : word;
begin
  getFAttr (a_file, temp);
  if dosError = 0 then
    exists := true
  else
    exists := false;
end;

procedure leftWrite (wall : byte; s : string);
begin
  if wall > length(s) then begin
    gotoXY (wall-length(s),whereY);
    write (s);
  end;
end;

procedure leftWriteLn (wall : word; s : string);
begin
  leftWrite(wall, s);
  writeLn;
end;

procedure centerWrite (center : byte; s : string);
begin
  gotoXY (center - (length(s) div 2), whereY);
  write  (s);
end;

procedure centerWriteLn (center : byte; s : string);
begin
  gotoXY (center - (length(s) div 2), whereY);
  writeLn  (s);
end;

function abc ( a : longint; b : byte) : string40;
var s : string;
begin
  str (a, s);
  while length (s) < b do
    s := '0'+ s;
  abc := s;
end;

function abcR ( a : real; b,c : byte) : string80;
var s : string;
begin
  str (a:0:c, s);
  while length (s) < b do
    s := '0'+ s;
  abcR := s;
end;

procedure turnCursor (on : boolean);
begin
  if on then
    asm
      mov ah,01;
      mov ch,8;
      mov cl,9;
      int $10;
    end
  else
    asm
      mov ah,01;
      mov ch,$20;
      mov cl,$20;
      int $10;
    end;
end;

function filter (obj, org, too : string) : string;
var b,b2 : byte;
    s,s2 : string;
begin
  b := 1;
  s := '';
  while length(obj)-b > 0 do begin;
    if length(obj)-b > length (org)
      then b2 := length (org)
      else b2 := length (obj)-b;
    b  := b + b2;
    s2 := copy(obj,b,b2);
    if s2 = org
      then s := s + too
      else s := s + s2;
  end;
  filter := s;
end;

function CTLfFilter (s : string) : string;
var b,b2 : byte;
begin
  while (s[1] = ' ') and (length(s) > 0) do
   s := copy (s,2,length(s)-1);
  while (s[length(s)] = ' ') and (length(s) > 0) do
   s := copy (s,1,length(s)-1);
  b2 := length(s);
  for b := b2 downto 1 do
   if s[b] = ';' then s := copy (s,1,b-1);
  CTLfFilter := s;
end;

function string2word (s : string40) : word;
var err : integer;
    tmp : word;
begin
  val(s,tmp,err);
  string2word := tmp;
end;

function integerIsEven (i : integer) : Boolean;
begin
  if (i div 2) = (i / 2)
  then integerIsEven := True
  else integerIsEven := false;
end;

function isUpcased (c : char) : boolean;
var bo : boolean;
begin
  bo := false;
  case c of
   'A'..'Z', '¸','ˇ','™','0','1'..'9' : bo := true;
  end;
  isUpcased := bo;
end;

procedure FX1Write (X,Y : byte; C1, C2 : Byte; DelayTime : word; s : String);
var a : integer;  m : byte;
begin
 s := ' '+s+' ';
 if not integerIsEven (length(s)) then s:= ' '+s;
 m := (length(s) div 2);
 for a := 1 to m do begin
   textColor(C1);
   gotoXY (X-a+1,y);
   write (s[m-a+1]);
   gotoXY (X+a-1,y);
   write (s[m+a-1]);

   delay(delayTime);
   gotoXY (X-a+1,y);
   textColor(C2);
   write (s[m-a+1]);
   gotoXY (X+a-1,y);
   write (s[m+a-1]);
 end;
 delay(delayTime*2);
end;

procedure FX2write (x,y, upc,downc : byte; delayTime : word; s : string);
var i : integer;
begin
 gotoXY(x,y);
 for i := 1 to length(s) do begin
  if isUpcased (s[i]) then if textAttr <> upc then textAttr := upc;
  if not isUpcased (s[i]) then if textAttr <> downc then textAttr := downc;
  write(s[i]);
  delay(delayTime);
 end;
end;


function upcase_s (s : string) : string;
var b : byte;
    q : string;
begin
 q := s;
 for b := 1 to length(q) do q[b] := upcase(q[b]);
 upcase_s := q;
end;

function input (max : byte) : string80;
var c : char;
    s : string;
    b : byte;
    x,
    y : byte;
begin
  s := '';
  c := ' ';
  x := whereX;
  y := whereY;
  repeat
    gotoXY (x,y);
    for b := 1 to max do
      if b <= length(s) then  write (s[b]) else write (' ');
    gotoXY (x+length(s),y);
    repeat until keypressed;
    c := readkey;
    case c of
      #00:  c := readkey;
      #08:  if s <> '' then s[0] := chr (length(s)-1);
      #27:  s := '';
      #32
      ..
      #255: if length(s) < max then s := s + c;
    end;
  until c = #13;
  input := s;
end;

function inputCode (max : byte) : string80;
var c : char;
    s : string;
    b : byte;
    x,
    y : byte;
begin
  s := '';
  c := ' ';
  x := whereX;
  y := whereY;
  repeat
    gotoXY (x,y);
    for b := 1 to max do
      if b <= length(s) then write('*') else write (' ');
    gotoXY (x+length(s),y);
    repeat until keypressed;
    c := readkey;
    case c of
      #00:  c := readkey;
      #08:  if s <> '' then s[0] := chr (length(s)-1);
      #27:  s := '';
      #32
      ..
      #255: if length(s) < max then s := s + c;
    end;
  until c = #13;
  inputCode := s;
end;

procedure wipe(s:string;echo:boolean);
var
  f: file;
  NumRead, NumWritten: Word;
  Buf: array[1..5*k] of byte;
  i :integer;
  fm : byte;
begin

 fm := filemode; {saving filemode}
 filemode := 2;  {read/write filemode}
 dosError := 0;

 {$I-}
 assign (f,s);
 setFAttr(f,Archive);
 {$I+}

 if FileExists(s) then begin
  if echo then write ('Wiping ',s,' ');
  assign (f,s);
  reset(f,1);
  repeat
    if echo then write ('.');
    BlockRead(f, Buf, SizeOf(Buf), NumRead);
    for i:= 1 to NumRead do buf[i] := 0;
    seek(f,filePos(f)-NumRead);
    BlockWrite(f, Buf, NumRead, NumWritten);
  until (NumRead = 0) or (NumWritten <> NumRead);
  Close(f);
  assign (f,s);
  reWrite(f);
  close  (f);
  assign (f,s);
  erase  (f);
  if echo then writeLn (' done.');
  {$I+}
 end;
 filemode := fm; {saved filemode}
end;


procedure bell (min,max : word);
var x : word;
procedure bell2;
begin
 sound(x);
 delay(x div 200);
end;

begin
 for x :=min to     max do if not keypressed then bell2;
 for x :=max downto min do if not keypressed then bell2;
 nosound;
end;

{
procedure BruteForce (var code : string);
var b : byte;
label nextChar;
begin
  b := length(code);
nextChar:
  if code[b] =  brute_highest then begin
     code[b] := brute_lowest;
     if b = 255 then halt (0);
     if b > 1 then begin
       dec(b);
       goto nextChar;
     end else
       code := code + brute_lowest ;
  end else
     code[b]:= chr(1+ord(code[b]));
end;}
procedure BruteForceAsm
          (var code : string; low, high : char); assembler;
asm
PUSH DS
LDS  BX,[BP+10]		{; get DS, string offset to BX}

xor     ch,ch
mov     cl,[bx]		{; length of string}
mov     dl,[BP+8]	{; lowest}
mov     dh,[BP+6]	{; highest}


@nextChar:
mov     si,bx		{;}
add     si,cx		{; si points to string[cx]}
cmp     [si],dh
jb      @charIsNotHighest
mov     [si],dl
cmp     cl,1
jbe     @incLength
loop    @nextChar
@incLength:
mov     cl,[bx]		{; string length}
inc	cl
mov	[bx],cl
add     bx,cx
mov     [bx],dl

jmp     @theEnd

@charIsNotHighest:
inc     byte ptr [si]
@theEnd:

POP DS
end;
procedure bootCold; assembler;
asm
 mov ax,40h
 mov ds,ax
 mov word ptr [0072h],0000
 {jmp $FFFF:$0000}
 db  $EA,$00,$00,$FF,$FF
end;
function HexWord(w: Word) : string40;
begin
 hexWord := (hexChars[Hi(w) shr 4]+
             hexChars[Hi(w) and $F]+
             hexChars[Lo(w) shr 4]+
             hexChars[Lo(w) and $F]);

end;

function Hexbyte(b: byte) : string40;
begin
 hexByte := (hexChars[b shr 4]+
             hexChars[b and $F]);

end;

procedure dispReg (reg : registers);
begin
 writeLn ('DS:SI ',hexword(reg.DS),':',hexword(reg.SI));
 writeLn ('ES:DI ',hexword(reg.ES),':',hexword(reg.DI));
 writeLn ('AX:   ',hexword(reg.AX));
 writeLn ('BX:   ',hexword(reg.BX));
 writeLn ('CX:   ',hexword(reg.CX));
 writeLn ('DX:   ',hexword(reg.DX));
 writeLn ('BP:   ',hexword(reg.BP));
 writeLn ('FLAG: ',hexword(reg.FLAGS));
end;

function MCB_para_detect (para : word) : Boolean;
var
 MCB                  : pMCB_Rec;
 InVarsSeg, InVarsOfs : Word;
 EnvSeg, Counter      : Word;
 tmp : boolean;
begin

  asm
    MOV   AH,52h
    INT   21h
    MOV   InVarsSeg,ES
    MOV   InVarsOfs,BX
  end;
  MCB := Ptr (MemW [InVarsSeg:InVarsOfs-2], 0);
 tmp := false;

 While MCB^.ChainID <> 90 do begin  { While valid MCBs exist... }
  If MCB^.Owner = Seg (MCB^) + 1 then { If MCB owns itself, then... }
  begin
   if MCB^.paragraphs = para then tmp := true;
  end;
  MCB := Ptr (Seg (MCB^) + MCB^.Paragraphs + 1, 0);
 end;
 MCB_para_detect := tmp;
end;

function readScreenXY (X,Y, len : byte) : String;
var b : byte; w : word; s : string; offset : word;
begin
 s := '';
 offset := ((y-1)*80 + (x-1)) * 2;
 for b := 1 to len do begin
  s := s + char(mem[$B800:offset]);
  inc(offset,2);
 end;
 readScreenXY := s;
end;
procedure charkb (outputChar : char); assembler;
  asm
    MOV     CH, 0
    MOV     CL, outputChar
    MOV     AH, 5
    INT     16H
  end;

procedure strkb (outputString : string80); assembler;
asm
 PUSH   DS
 LDS    SI, outputString
 MOV    CH,0
 LODSB
 MOV    CL, AL
 @loopen:
 LODSB
 MOV    BL, CL
 MOV    CL, AL
 MOV    AH, 5
 INT    16H
 MOV    CL, BL
 LOOP   @loopen
 POP    DS
end;

function CTLread (var CTLf : text) : string;
var CTLs : string;
begin
  repeat
    readLn(CTLf,CTLs);
  until (CTLs <> '') or (eof(CTLf));
  CTLread := CTLfFilter(CTLs);
end;

procedure phasor (times : word); assembler;
asm
        pusha
        mov cx, times
@phasor:
        push    cx

        cli
        mov     dx,12000
        sub     dx,cs:5000
        mov     bx,100
        mov     al,10110110b
        out     43h,al
@backx:
        mov     ax,bx
        out     42h,al
        mov     al,ah
        out     42h,al
        in      al,61h
        mov     ah,0
        or      ax,00000011b
        out     61h,al
        inc     bx
        mov     cx,15
@loopx:
        loop    @loopx
        dec     dx
        cmp     dx,0
        jnz     @backx
        in      al,61h
        and     al,11111100b
        out     61h,al
        sti
        pop     cx
        loop    @phasor
        popa
end;

function exeDir : string80;
var s, result   : string80;
    b           : byte;
begin
 s := fexpand(paramStr(0));
 result := '';
 for b := 1 to length(s) do
  if (s[b] = '\') or (s[b] = ':') then result := copy(s,1,b);
 if result[length(result)] = '\' then dec(byte(result[0]));
 if (length(result) = 2) and (result[2] = ':') then result := result+'\';
 exeDir := result;
end;

function exePath : string80;
var s, result    : string80;
    b            : byte;
begin
 s := fexpand(paramStr(0));
 result := '';
 for b := 1 to length(s) do
  if (s[b] = '\') or (s[b] = ':') then result := copy(s,1,b);
 if result[length(result)] <> '\' then result := result+'\';
 exePath := result;
end;

procedure exe (cmd, param : string);
begin
 swapVectors;
 exec (cmd,param);
 swapVectors;
end;

procedure writer (s : string);
var con : text;
begin
 assign (con,'');
 rewrite(con);
 write(con,s);
 close(con);
end;

procedure writerLn (s : string);
begin
 writer(s+rad);
end;

procedure write_ViaDOS;
begin
 assign  (output,'');
 rewrite (output);
end;

function noPath (fileName : pchar) : pchar; assembler;
var _es, _di : word;
asm
 pusha
 lds   si, fileName
 les   di, fileName
 @l:
 lodsb
 cmp   al,0
 je    @finished
 cmp   al,'\'
 je    @l2
 cmp   al,':'
 je    @l2
 jmp   @l
 @l2:
 mov   di,si
 jmp   @l

 @finished:
 mov _es, es
 mov _di, di
 popa
 mov ax, _di
 mov dx, _es
end;

function FExists (fil : string) : boolean;
begin
 FExistsLast := FSearch(fil,'');
 if FExistsLast <> '' then begin
  FExists := True;
  FExistsLast  := FExpand(FExistsLast);
  FExistsMatch := FExistsLast;
 end else FExists := false;
end;

function CurPath : string80;
begin
 CurPath := fexpand('');
end;

function CurDir : string80;
var s : string;
begin
 getDir(0, s);
 CurDir := s;
end;

function randomStr (minChar,maxChar : char; minLen, maxLen : byte) : string;
var b : byte; tst : string;
begin
 for b := 1 to minLen+random(1+maxLen-minLen) do tst[b] := char(byte(minChar)+random(1+byte(maxChar)-byte(minChar)));
 tst[0] := char(b);
 randomStr := tst;
end;

function paddStrLeft ( s : string; paddchar : char; b : byte) : string;
begin
  while length (s) < b do
    s := paddchar+ s;
  paddStrLeft := s;
end;

function paddStrRight (s : string; paddchar : char; b : byte) : string;
begin
  while length (s) < b do
    s := s +paddchar;
  paddStrRight := s;
end;

function downcase (c : char) : char;
begin
 case c of
  'A'..'Z' : downcase := char(byte(c)+32);
  else downcase := c;
 end
end;

function downcase_s (s : string) : string;
var b : byte;
    q : string;
begin
 q := s;
 for b := 1 to length(q) do q[b] := downcase(q[b]);
 downcase_s := q;
end;

function upcasePath (s : string) : string;
const spec : boolean = true;
var b : byte;
begin
 for b := 1 to length(s) do begin
  if b > 1 then
   if spec
   then s[b] := upcase(s[b])
   else s[b] := downcase(s[b])
  else  s[b] := upcase(s[b]);
  case s[b] of
   #0..#64, #91..#96, #123..#127, #169..#255 : spec := true;
   else spec := false;
  end;
 end;
 upcasePath := s;
end;

procedure displayXY (x, y : word; forground, background : byte; s : string); assembler;
asm
 push ds
 LDS  BX,s

 mov dh, background
 mov cl, 4
 shl dh, cl
 mov dl, forground
 or  dh, dl
 push dx

 mov ax, y
 dec ax
 mov cx, 80
 mul cx
 add ax, x
 dec ax
 mov cx, 2
 mul cx
 mov di, ax

 mov ax,0B800h
 mov es,ax
 mov si,bx  (* pekare till s *)
 inc si
 xor ch,ch  (* ch = 0 *)
 mov cl,[bx]
 pop dx
 cld
 @lab:
  movsb
  mov es:[di],dh
  inc di
 loop @lab
 pop ds
end;

function diskInDrive (drive : char) : boolean;
begin
 drive := upcase (drive);
 if diskfree (byte(drive) - byte('A') +1) = - 1
 then diskInDrive := false
 else diskInDrive := true;
end;

{
procedure cmosByteWrite (cAddr, cVal : byte); assembler;
asm
 mov dx, 070h
 mov al, cAddr
 out dx, al
 inc dx
 mov al, cVal
 out dx, al
end;

function cmosByteRead (cAddr : byte) : byte; assembler;
asm
 mov dx, 070h
 mov al, cAddr
 out dx, al
 inc dx
 in  al, dx
end;
}

procedure DosShell (cmd : string);
begin
 exe(getEnv('COMSPEC'),'/C '+cmd);
end;

function getThePath(s : string80) : string80;
var result : string;
    b      : byte;
begin
 result := '';
 for b := 1 to length(s) do
  if (s[b] = '\') or (s[b] = ':') then result := copy(s,1,b);
{ if length(result) > 0 then
  if result[length(result)] <> '\' then result := result+'\';}
 getThePath := result;
end;

procedure getDiskInfo (drive : byte; var diskinfo : DiskInfoR); assembler;
asm
   pusha
   mov ah,69h
   mov al,00h
   mov bl,drive
   mov bh,00h
   lds dx,diskinfo
   int 21h
   popa
end;

function binWord ( w : word ) : string40; assembler;
asm
 cld
 mov  dx, es
 les  di, ss:[BP+8]
 mov  al, 16
 stosb
 mov  ax, 32768
 mov  bx, w
@l:
 test bx, ax
 jz   @l0

 push ax
 mov  al, '1'
 stosb
 pop ax
 jmp @div2
@l0:
 push ax
 mov  al, '0'
 stosb
 pop ax

@div2:
 shr  ax, 1
 cmp  ax, 0
 jne  @l

 mov  es, dx
end;

function binByte ( b : byte ) : string40; assembler;
asm
 cld
 mov  dx, es
 les  di, ss:[BP+8]
 mov  al, 8
 stosb
 mov  al, 128
 mov  bl, b
@l:
 test bl, al
 jz   @l0

 push ax
 mov  al, '1'
 stosb
 pop  ax
 jmp  @div2
@l0:
 push ax
 mov  al, '0'
 stosb
 pop  ax

@div2:
 shr  al, 1
 cmp  al, 0
 jne  @l

 mov  es, dx
end;

{
function binByte ( b : byte ) : string;
var
 cnt : byte;
 s   : string[8];
begin
 s := '';
 cnt := 128;
 repeat
  if b and cnt <> 0
  then s := s + '1'
  else s := s + '0';
  cnt := cnt div 2;
 until cnt = 0;
 binByte := s;
end;
}

function binInteger ( i : integer ) : string40; assembler;
asm
 cld
 mov  dx, es
 les  di, ss:[BP+8]
 mov  al, 16
 stosb
 mov  ax, 32768
 mov  bx, i
 test bx, ax
 jnz  @minus
 push ax
 mov  al, '+'
 stosb
 pop  ax
 jmp  @AfterPlusMinus

@minus:
 push ax
 mov  al, '-'
 stosb
 pop  ax

@AfterPlusMinus:
 shr  ax, 1

@l:
 test bx, ax
 jz   @l0

 push ax
 mov  al, '1'
 stosb
 pop ax
 jmp @div2
@l0:
 push ax
 mov  al, '0'
 stosb
 pop ax

@div2:
 shr  ax, 1
 cmp  ax, 0
 jne  @l

 mov  es, dx
end;

function bin2word ( s : string ) : word; assembler;
asm
 push  ds
 lds   si, s
 mov   al, [si]
 cmp   al, 0
 je    @exit
 cmp   al, 16
 ja    @error
 xor   ch,ch
 mov   cl,al
 add   si,cx
 xor   bx,bx
 mov   dx,1
 std
@l:
 lodsb
 cmp   al,'0'
 je    @noll
 cmp   al,'1'
 je    @ett
 jmp   @error

@ett:
 or    bx, dx
@noll:
 shl   dx, 1
 loop  @l

 mov   ax,bx
 jmp   @exit

@error:
 xor ax, ax
@exit:
 pop ds
 cld
end;


function StrParam (prm : string; index : byte) : string;
var
 s   : string;
 b,
 cnt : byte;
begin
 s := '';
 cnt := 0;
 prm := ' '+prm;
 for b := 2 to length(prm) do begin
   if (prm[b - 1] = #32) and (prm[b] <> #32) then inc(cnt);
   if cnt = index then
    if prm[b] <> #32 then s := s+prm[b];
 end;
 StrParam := s;
end;

function YesOrNo(default : YesNo) : YesNo;
var c : char;
begin
 if Default = No
 then write (' [y/N] ')
 else write (' [Y/n] ');
 repeat
  c:= upcase(readkey);
  if c = #0 then readkey;
 until (c = #13) or (c = 'N') or (c = 'Y');
 case c of
  'N' : YesOrNo := No;
  'Y' : YesOrNo := Yes;
  #13 : YesOrNo := Default;
 end;
end;

function  byte2word (hi, lo : byte) : word; assembler;
asm
 mov ah,hi
 mov al,lo
end;

procedure EliteWrite(s : string);

var
   tmpb : byte;
   st   : string;

begin
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
end;


procedure EliteCenterWrite(center :byte;s : string);

var
   tmpb : byte;
   st   : string;

begin
gotoXY (center - (length(s) div 2), whereY);
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
end;

procedure EliteLeftWrite(wall : byte;s : string);

var
   tmpb : byte;
   st   : string;

begin
if wall > length(s) then begin
  gotoXY (wall-length(s),whereY);
end;
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
end;

procedure EliteWriteLn(s : string);

var
   tmpb : byte;
   st   : string;

begin
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
    Writeln;
end;

procedure EliteCenterWriteLn(center :byte;s : string);

var
   tmpb : byte;
   st   : string;

begin
gotoXY (center - (length(s) div 2), whereY);
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
    writeln;
end;

procedure EliteLeftWriteLn(wall : byte;s : string);
var
   tmpb : byte;
   st   : string;

begin
if wall > length(s) then begin
  gotoXY (wall-length(s),whereY);
end;
s := downcase_s(s);
for tmpb := 1 to length(s) do begin
    case s[tmpb] of
      'a','e','i','o','u','y',
      '†','„','”': begin
      textcolor(8);
      write(s[tmpb]);
      end;
      'b','c','d','f','g','h',
      'j','k','l','m','n','p',
      'q','r','s','t','v','w',
      'x','z': begin
      textcolor(15);
      write(upcase(s[tmpb]));
      end;
      '0'..'9': begin
      textcolor(15);
      write(s[tmpb]);
      end;
      #32..#47,#58..#96,
      #123..#131,#133,
      #135..#147,
      #149..#255: begin
      textcolor(8);
      write(s[tmpb]);
      end;
    end;
    end;
    writeln;
end;



{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure SetVGA_Pal256 (Col,R,G,B : Byte); assembler;
  { This sets the Red, Green and Blue values of a certain color }
asm
   mov    dx,3c8h
   mov    al,[col]
   out    dx,al
   inc    dx
   mov    al,[r]
   out    dx,al
   mov    al,[g]
   out    dx,al
   mov    al,[b]
   out    dx,al
end;

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure GetVGA_Pal256 (Col : Byte; Var R,G,B : Byte);
  { This gets the Red, Green and Blue values of a certain color }
Var
   rr,gg,bb : Byte;
Begin
   asm
      mov    dx,3c7h
      mov    al,col
      out    dx,al

      add    dx,2

      in     al,dx
      mov    [rr],al
      in     al,dx
      mov    [gg],al
      in     al,dx
      mov    [bb],al
   end;
   r := rr;
   g := gg;
   b := bb;
end;

function dateYYYYMM : longint;
var
 Year, Month, w : word;
begin
 getdate (year,month,w,w);
 dateYYYYMM := longint(year)*100+month;
end;


function tmpPath : string80;
var
 TempDir : string80;
begin
 TempDir := getEnv('TEMP');
 if TempDir = '' then
  TempDir := getEnv('TMP');
 if TempDir = '' then
  TempDir := 'C:';
 if TempDir[length(TempDir)] <> '\' then TempDir := TempDir + '\';
 tmpPath := TempDir;
end;

Procedure LightBarTextScroll
          ( x,y : byte; s : string80; Col1,Col2,Col3,Col4 : byte; DelayT : Integer);
var
 goingRight : boolean;
 pos : byte;

 procedure LightBarTextScroll_sub;
 begin
  if not keypressed then begin
   gotoXY(x,y);
   textColor (col4);
   write (copy(s,1,pos-3));
   textColor (col3);
   write (copy(s,pos-2,1));
   textColor (col2);
   write (copy(s,pos-1,1));
   textColor (col1);
   write(s[pos]);
   textColor (col2);
   write (copy(s,pos+1,1));
   textColor (col3);
   write (copy(s,pos+2,1));
   textColor (col4);
   write (copy(s,pos+3,length(s)-pos-2));
   delay(delayt);
  end;
 end;

begin
 repeat
  for pos := length(s)-2-1 downto 3 do LightBarTextScroll_sub;
  for pos := 3+1 to length(s)-2 do LightBarTextScroll_sub;
 until keypressed;
 while keypressed do readkey;
end;





Procedure LightBarTextScroll_Double
          ( x,y : byte; s1,s2 : string80; Col1,Col2,Col3,Col4 : byte; DelayT : Integer);
var
 goingRight : boolean;
 p1,p2 : byte;
 d1,d2 : boolean;

 procedure LightBarTextScroll_sub (s : string80; pos : byte);
 begin
  if not keypressed then begin
   gotoXY(x-(length(s) div 2),whereY);
   textColor (col4);
   write (copy(s,1,pos-3));
   textColor (col3);
   write (copy(s,pos-2,1));
   textColor (col2);
   write (copy(s,pos-1,1));
   textColor (col1);
   write(s[pos]);
   textColor (col2);
   write (copy(s,pos+1,1));
   textColor (col3);
   write (copy(s,pos+2,1));
   textColor (col4);
   write (copy(s,pos+3,length(s)-pos-2));
  end;
 end;

begin
 p1 := length(s1)-3; d1 := false;
 p2 := 3; d2 := true;

 repeat
  if d1 = true then
   if p1 = 3 then d1 := false;
  if d1 = false then
   if p1 = length(s1)-2 then d1 := true;
  if d1 = true
  then dec(p1)
  else inc(p1);

  if d2 = true then
   if p2 = 3 then d2 := false;
  if d2 = false then
   if p2 = length(s2)-2 then d2 := true;
  if d2 = true
  then dec(p2)
  else inc(p2);

  gotoXY(1, y);
  LightBarTextScroll_sub(s1,p1); writeLn;
  LightBarTextScroll_sub(s2,p2); writeLn;
  delay(delayt);
 until keypressed;
 while keypressed do readkey;
end;


procedure WaitRetrace; assembler;
asm
    mov dx,3DAh
@l1:
    in al,dx
    and al,08h
    jnz @l1
@l2:
    in al,dx
    and al,08h
    jz  @l2
end;


function rorw ( w, cnt : word ) : word; assembler;
asm
 mov ax, w
 mov cx, cnt
@l:
 rep ror ax, 1
 loop @l
end;
function rolw ( w, cnt : word ) : word; assembler;
asm
 mov ax, w
 mov cx, cnt
@l:
 rol ax, 1
 loop @l
end;

function rorb ( b : byte; cnt : word ) : byte; assembler;
asm
 mov al, b
 mov cx, cnt
@l:
 ror al, 1
 loop @l
end;
function rolb ( b : byte; cnt : word ) : byte; assembler;
asm
 mov al, b
 mov cx, cnt
@l:
 rol al, 1
 loop @l
end;



procedure shutdown; assembler;
const
 FLUSHWAIT = 2;
asm
                push  cs
                pop   ds
                mov   dx,offset @m_newline
                mov   ah,9
                int   21h


 { *** first check if smartdrv is active and if so,
   *** flush it's caches (only for smartdrv v4+)     }

                xor   bx,bx             { installation check }
                mov   cx,0ebabh
                mov   ax,4a10h
                int   2fh
                cmp   ax,0babeh
                jne   @apm

                mov   dx,offset @m_flushcache  { just a short msg to the user }
                mov   ah,9
                int   21h

                mov   bx,1              { flush the caches }
                mov   ax,4a10h
                int   2fh

                cli                     { reset the pit, just to be sure }
                mov   al,36h
                out   43h,al
                xor   ax,ax
                out   40h,al
                out   40h,al
                sti
                mov   es,ax
                mov   [es:046ch],ax     { wait some time (s.a.) }
                mov   ax,FLUSHWAIT*19
@waitloop:      cmp   ax,[es:046ch]
                ja    @waitloop

{
 ; *** second, check if the BIOS has the APM-extensions
 ; *** and if yes, try to power-off
}

@apm:           xor   bx,bx             { installation check }
                mov   ax,5300h
                int   15h
                jc    @dopoweroff
                cmp   bx,504dh
                jne   @dopoweroff

                xor   bx,bx             { disconnect any other client }
                mov   ax,5304h
                int   15h

                xor   bx,bx             { real-mode connect }
                mov   ax,5301h
                int   15h
                jc    @dopoweroff

                mov   bx,0001h          { system shutdown }
                mov   cx,0003h
                mov   ax,5307h
                int   15h

{
 ; *** third, if there is no APM-BIOS or the power can't be
 ; *** switched off automatically, tell the user to do it...
}

@dopoweroff:    mov   dx,offset @m_dopoweroff  { another short user-msg }
                mov   ah,9
                int   21h

{@endless:       jmp   @endless}
jmp @exit;

{ ; *** the program's messages}

@m_flushcache:
    db    'Flushing the SmartDrive caches...',13,10
    db    'Please wait for the drives to finish writing...'
@m_newline:
    db    13,10,36
@m_dopoweroff:
    db    'System cannot be automaticly downed, trun off manually...',36


{ ; *** that's all folks!}
@exit:
end;



function scan2ascii (c : char) : char;
{
 input:   one character SCANCODE
 returns: the character converted to ASCII
}
begin
  case c of
   #30: scan2ascii := 'A';
   #48: scan2ascii := 'B';
   #46: scan2ascii := 'C';
   #32: scan2ascii := 'D';
   #18: scan2ascii := 'E';
   #33: scan2ascii := 'F';
   #34: scan2ascii := 'G';
   #35: scan2ascii := 'H';
   #23: scan2ascii := 'I';
   #36: scan2ascii := 'J';
   #37: scan2ascii := 'K';
   #38: scan2ascii := 'L';
   #50: scan2ascii := 'M';
   #49: scan2ascii := 'N';
   #24: scan2ascii := 'O';
   #25: scan2ascii := 'P';
   #16: scan2ascii := 'Q';
   #19: scan2ascii := 'R';
   #31: scan2ascii := 'S';
   #20: scan2ascii := 'T';
   #22: scan2ascii := 'U';
   #47: scan2ascii := 'V';
   #17: scan2ascii := 'W';
   #45: scan2ascii := 'X';
   #21: scan2ascii := 'Y';
   #44: scan2ascii := 'Z';
   #02: scan2ascii := '1';
   #03: scan2ascii := '2';
   #04: scan2ascii := '3';
   #05: scan2ascii := '4';
   #06: scan2ascii := '5';
   #07: scan2ascii := '6';
   #08: scan2ascii := '7';
   #09: scan2ascii := '8';
   #10: scan2ascii := '9';
   #11: scan2ascii := '0';
   else scan2ascii := c;
  end;
end;


function removeExtension (fn : openstring) : string80;
var s : string80;
begin
 s := fn;
 while (length(s[0]) <> 0) and (s[length(s)] <> '.') do dec(s[0]);
 if s[length(s)] = '.' then dec(s[0]);
 removeExtension := s;
end;

function removePath (fn : openstring) : string12;
var
 b, i : byte;
{ s    : string12;}
begin
 b := 1;
 for i := 1 to length(fn) do
  case fn[i] of ':', '\': b := i;
  end;
 removePath := copy (fn, b, 12);
end;


