  const
    APPSV    = '980429.1';
    _c       = '(c)';
    _v       = 'Version';
    _fw      = 'FreeWare';
    _sw      = 'ShareWare';
    _pd      = 'PublicDomain';
    Yes = True;
    No  = False;
    On  = True;
    Off = False;
    k   = 1024;    { a kilobyte }
    M   = k*k;     { a megabyte }
    G   = k*k*k;   { a gigabyte }
    rad = #13+#10; { EOL for MS-DOS }
    hexChars: array [0..$F] of Char = '0123456789ABCDEF'; { hexadecimal table }

Type YesNo = boolean;
Type pMCB_Rec = ^MCB_Rec;                                     { ripped: Sorry, origin unkown }
     MCB_Rec  = Record
     ChainID    : Byte; { 77 if part of MCB chain, 90 if last MCB allocated }
     Owner      : Word; { PSP segment address of the MCB's owner }
     Paragraphs : Word; { Paragraphs related to this MCB }
     dummy      : array [0..2] of byte;
     name       : array [0..7] of char;
end;
Type DiskInfoR  = record
     infoLevel,
     seriLow,
     seriHi     : word;
     diskLabel  : array[1..11] of char;
     fileSys    : array[1..8] of char;
end;
type
 string8   = string[8];
 string12  = string[12];
 string20  = string[20];
 string40  = string[40];
 string60  = string[60];
 string80  = string[80];
 string100 = string[100];

  {converting}
  function abc ( a : longInt; b : byte) : string40;
  function abcR ( a : real; b,c : byte) : string80;
  function string2word (s : string40) : word;
  function byte2word (hi, lo : byte) : word;
  function HexByte    (b : Byte    ) : string40; { number -> hexString }
  function HexWord    (w : Word    ) : string40;
  function binWord    (w : word    ) : string40; { number -> bit String }
  function binByte    (b : byte    ) : string40;
  function binInteger (i : integer ) : string40; { not very good... use binWord if possible }
  function bin2word   (s : string  ) : word;   { bit String -> number }
  function scan2ascii (c : char) : char;    { scancode -> ascii }

  {string manipulation}
  function upcase_s (s : string) : string;
  function downcase (c : char) : char;
  function downcase_s (s : string) : string;
  function upcasePath (s : string) : string; { Upcase char after spec. char }
  function paddStrLeft  (s : string; paddchar : char; b : byte) : string;
  function paddStrRight (s : string; paddchar : char; b : byte) : string;

  {checking}
  function integerIsEven (i : integer) : Boolean;
  function isUpcased (c : char) : boolean;

  {bit manipulation}
  function rorw ( w, cnt : word ) : word;
  function rolw ( w, cnt : word ) : word;
  function rorb ( b : byte; cnt : word ) : byte;
  function rolb ( b : byte; cnt : word ) : byte;

  {DOS}
  procedure exe (cmd, param : string);
  procedure DosShell (cmd : string);
  function exeDir  : string80; { Usage: chDir(exeDir);         }
  function CurDir  : string80;
  function exePath : string80; { Usage: exeDir+'FILENAME.EXT'; }
  function CurPath : string80;
  function TmpPath : string80;
                      {        C:\     C:\DIR\SUB
                        Dir:   C:\     C:\DIR\SUB
                        Path:  C:\     C:\DIR\SUB\  <-- Not the same!   }
  function getThePath(s : string80) : string80;
  function noPath (fileName : pchar) : pchar; { removes path from fileName }
  function removePath (fn : openstring) : string12; { removes path from fn }
  function removeExtension (fn : openstring) : string80;
  function diskInDrive (drive : char) : boolean;
  function StrParam (prm : string; index : byte) : string;
           { Like ParamStr but you choose from what string to check from }

  {simple keyboard}
  function input     (max : byte) : string80;
  function inputCode (max : byte) : string80;
  function YesOrNo(default : YesNo) : YesNo;
  {fake keypresses}
  procedure charkb (outputChar : char);
  procedure strkb  (outputString : string80);

  {security}
  function randomStr (minChar,maxChar : char; minLen, maxLen : byte) : string;
            {randomStr creates a random string... for random passwords etc }
  procedure BruteForceAsm (var code : string; low, high : char);
  procedure wipe (s:string;echo:boolean);

  {hardware and memory access}
  function MCB_para_detect (para : word) : Boolean;
  procedure bootCold;                                    { ripped: terminate }
  procedure shutdown;


  {file access}
  function FileExists(FileName: string) : Boolean;       { ripped: borland - bugfixed by me 961231 }
  function FileExistsSHARE(FileName: string) : Boolean;
  function FExists (fil : string) : boolean;
  var      FExistsMatch,FExistsLast : string;
  function filter (obj, org, too : string) : string;
  function CTLfFilter (s : string) : string;
  function CTLread (var CTLf : text) : string;

  {sound}
  procedure bell (min,max : word);
  procedure phasor (times : word);                       { ripped: ivp }

  {screen}
  function  readScreenXY (X,Y, len : byte) : String;
  procedure displayXY (x, y : word; forground, background : byte; s : string);  { DisplayXY writes directly to text-RAM }
  procedure turnCursor (on : boolean);
  procedure write_ViaDOS; { Make all writes via dos standard }
  Procedure GetVGA_Pal256 (Col : Byte; Var R,G,B : Byte);  { ripped: GFX.PAS }
  Procedure SetVGA_Pal256 (Col,R,G,B : Byte);              { ripped: GFX.PAS }
  procedure WaitRetrace;                                   { ripped: GFX.PAS }

  { Hardware, BIOS & similar information }
  procedure getDiskInfo (drive : byte; var diskinfo : DiskInfoR);

  {

  **** Note! the cmos* support has been removed from apps.  ****
  **** The propper way to support cmos in the future is to  ****
  **** use cmos.pas (aka cmos.tpu)                          ****

  procedure cmosByteWrite (cAddr, cVal : byte);
  function  cmosByteRead (cAddr : byte) : byte;

  }

  { Special FX write procedures }
  procedure FX1write (X,Y, C1,  C2    : Byte; DelayTime : word; s : string);
  procedure FX2write (X,Y, upC, downC : byte; delayTime : word; s : string);
  procedure leftWrite     (wall : byte; s : string);
  procedure leftWriteLn   (wall : word; s : string);
  procedure centerWrite   (center : byte; s : string);
  procedure centerWriteLn (center : byte; s : string);
  procedure EliteWrite         (s : string);
  procedure EliteWriteLn       (s : string);
  procedure EliteCenterWrite   (center : byte;s : string);
  procedure EliteCenterWriteLn (center : byte;s : string);
  procedure EliteLeftWrite     (wall : byte;s : string);
  procedure EliteLeftWriteLn   (wall : byte;s : string);
  procedure LightBarTextScroll
            ( x,y : byte; s : string80; Col1,Col2,Col3,Col4 : byte; DelayT : Integer);
  procedure LightBarTextScroll_Double
            ( x,y : byte; s1,s2 : string80; Col1,Col2,Col3,Col4 : byte; DelayT : Integer);


  {misc}
  procedure dispReg (reg : registers);
  function dateYYYYMM : longint; { more useful getDate }


  { Old crap }
  procedure writer   (s : string);   { Output via dos standard - bugfixed }
  procedure writerLn (s : string);   { Output via dos standard - bugfixed }

  { ----------------------------------------------------------------------}

  {
  Brute_Highest = 'Z';
  Brute_Lowest  = 'A';
  procedure BruteForce (var code : string);
  }
