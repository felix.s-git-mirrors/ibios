unit biodump;
{$I-}

interface
 procedure romdump (fn : string);
implementation

procedure romdump;

const
 k    = 1024;
 max  = 255;
 mmax = $FFFF;
 rad  = #13+#10;
type romRec = array[0..max] of byte;
var
 rom : ^romRec;
 l   : word;
 i   : integer;
 f   : file;

begin
 new(rom);
 assign (f, fn);
 rewrite(f,1);
 i := 0;
 for l := $0000 to mmax do begin
  rom^[i] := mem[$F000:l];
  inc(i);
  if (i = sizeOf(rom^)) or (l = mmax) then blockwrite(f,rom^,i);
  if i = sizeOf(rom^) then i := 0;
 end;
 close(f);
 dispose(rom);
end;
end.

