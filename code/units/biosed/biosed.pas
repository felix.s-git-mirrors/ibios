unit biosed;
(*
  This is a very crude editor of cmos,
  featuring help from CMOS.LST and ability to correct checksums.
*)

interface
{----------------------------------------------------------}
uses
 crt,dos,  { comes with borland pascal }
 apps,     { various usefull functions }
 cmos;     { cmos-access features      }

procedure simpleCmosEditor;
{----------------------------------------------------------}
implementation

const version = '0.01�';
procedure simpleCmosEditor;

var
 cmos, cmosold : array [0..127] of byte;
 x,y : byte;

procedure grep (fn,s : string);
var f : text; t : string; i : integer;
begin
 if not fileExists(exepath+fn) then begin
  if not fileExists(fn)
   then begin
     writeLn ('Place '+fn+' in current directory for extended help!');
     exit;
   end else assign (f, fn)
 end else assign (f,exepath+fn);
 reset(f);
 while not eof(f) do begin
  readln (f, t);
  if s=copy(t,1,length(s)) then begin
   write(copy(t,1,79));
   for i := whereX to 79 do write(' ');
   writeLn;
  end;
 end;
 close(f);
end;

procedure calculatePCAT;
var
 w : word;
 i,j : integer;
begin
 w := 0;
 for i := $10 to $2D do w:= w+cmos[i];
 cmos[$2E] := hi(w);
 cmos[$2F] := lo(w);
end;

procedure calculatePS2;
var
 w : word;
 i,j : integer;
begin
 w := 0;
 for i := $10 to $18 do w:= w+cmos[i];
 cmos[$2E] := hi(w);
 cmos[$2F] := lo(w);
end;

procedure calculateHPVectra;
var
 w : word;
 i,j : integer;
begin
 w := 0;
 for i := $10 to $20 do w:= w+cmos[i];
 cmos[$2E] := hi(w);
 cmos[$2F] := lo(w);
 w := 0;
 for i := $29 to $2D do w:= w+cmos[i];
 cmos[$28] := lo(w);
end;

procedure calculateAwardE;
var
 w : word;
 i,j : integer;
begin
 w := 0;
 for i := $40 to $79 do w:= w+cmos[i];
 cmos[$7A] := hi(w);
 cmos[$7B] := lo(w);
end;

procedure calculateAwardE2;
var
 w : word;
 i,j : integer;
begin
 w := 0;
 for i := $40 to $7C do w:= w+cmos[i];
 cmos[$7D] := hi(w);
 cmos[$7E] := lo(w);
end;

procedure initscrn1;
var i, j : integer;
begin
 TextBackground(black);
 clrScr;
 TextColor(white);
 writeLn ('   SCEBB: Simple CMOS Editor by Bluefish, version '+version);
 TextColor(green);
 gotoxy(4,3);
 for i := 0 to $F do write (hexbyte(i)+' ');
 gotoxy(1,4);
 for i := 0 to 7 do writeLn (hexbyte(i*16));
 gotoxy(1,whereY+3);

 write  ('Enter: Enter a decimal value');
 gotoXY(35,whereY);
 writeLn('F1: Calculate IBM PC/AT standard checksum');

 write  ('Space: Enter a hexadecimal value');
 gotoXY(35,whereY);
 writeLn('F2: Calculate IBM PS/2 checksum');

 write  ('0 - 7: Toggle one bit');
 gotoXY(35,whereY);
 writeLn('F3: Calculate HP Vectra checksums');

 write  ('L    : Reload from CMOS');
 gotoXY(35,whereY);
 writeLn('F4: Calculate Award extended checksums');

 write  ('S    : Save to CMOS');
 gotoXY(35,whereY);
 writeLn('F5: Calculate Award extended checksums v2');
end;

procedure displayscreen;
var i,j,idx : integer;
begin
 for i := 0 to 7 do
  for j := 0 to $F do begin
   idx := i*16+j;
   gotoxy (j+55,i+4);
   if cmos[idx] = cmosold[idx]
   then textColor(lightgray)
   else textcolor(white);
   if (x=j) and (y=i) then textcolor(yellow);
   if cmos[idx] >= 32 then write(chr(cmos[idx])) else write('.');
   gotoxy (j*3+4,i+4);
   write (hexbyte(cmos[idx]));
  end;
 idx := y*16+x;
 textColor(green);
 gotoxy (1,13);
 writeLn ('cmos[$'+hexbyte(idx)+']: $'+
   hexbyte(cmos[idx])+' = B'+
   binbyte(cmos[idx])+' = '+abc(cmos[idx],3)+'   ');
 gotoXY (1,22);
 grep('CMOS.LST','CMOS '+hexbyte(idx));
end;

procedure getcmosdata;
var i : integer;
begin
 for i := 0 to 127 do cmos[i] := cmosByteRead(i);
 cmosold := cmos;
end;

procedure saveCmosData;
var c : char; i : integer;
begin
 clrScr;
 writeLn;
 writeLn ('"I hearby sell my soul to Bluefish and revoke all my legal rights'+rad+
          ' in any case related to Bluefish or the Eleventh Alliance"'+rad);
 writeLn ('"I admit that I know that the application I have used is only intended'+rad+
          ' for certified (by Bluefish) engineers and experienced hardware hackers,'+rad+
          ' and that I''m none of those things."'+rad+rad);
 write ('Have you sworn those oaths and sold your soul to us? [Yes/No]');
 repeat
  c := upcase(readkey);
  case c of
   #00: readkey;
   'N': Exit;
  end;
 until c = 'Y';
 write (' Yes'+rad+'Do you wish to save, knowing that your changes may be fatal? [Yes/No]');
 repeat
  c := upcase(readkey);
  case c of
   #00: readkey;
   'N': Exit;
  end;
 until c = 'Y';
 for i := 0 to 127 do cmosByteWrite(i,cmos[i]);
 writeLn (' Yes'+rad+rad+'Changes has been saved to CMOS.');
 if readkey = #0 then readkey;
 getcmosdata;
end;

function hex2long(s:string20) : longint;
var
 a,b,c : longint;
begin
 a := 1;
 c := 0;
 s := upcase_s(s);
 for b := length(s) downto 1 do
  case s[b] of
   '0'..'9': begin
     c := c + a*longint( ord(s[b]) - ord('0') );
     a := a shl 4;
    end;
   'A'..'F': begin
     c := c + a*longint( ord(s[b]) - ord('A') + 10 );
     a := a shl 4;
    end;
   end;
 hex2long := c;
end;

var w : integer; c : char; idx : word; s:string;
begin
 w := lastMode;
 x := 0;
 y := 0;
 textMode(C80 + Font8x8);
 turnCursor(false);
 initscrn1;
 getcmosdata;
 repeat
  idx := y*16+x;
  displayscreen;
  c := upcase(readkey);
  case c of
    '0'..'7': begin
               cmos[idx] := cmos[idx] xor (1 SHL (ord(c) - ord ('0')));
              end;
    #13: begin
          gotoXY (1,20);
          textColor(cyan);
          write ('Enter a decimal number [range 0 to 255]: ');
          textColor(lightcyan);
          readln(cmos[idx]);
          initscrn1;
         end;
    #32: begin
          gotoXY (1,20);
          textColor(cyan);
          write ('Enter a hexadecimal number [range $00 to $FF]: ');
          textColor(lightcyan);
          readln(s);
          cmos[idx] := hex2long(s);
          initscrn1;
         end;
    'S': begin
           savecmosdata;
           initscrn1;
         end;
    'L': getCmosData;
    #27: begin
          textMode(w);
          ClrScr;
          Exit;
         End;
    #00:
     case readkey of
       #59: calculatePCAT;
       #60: calculatePS2;
       #61: calculateHPVectra;
       #62: calculateAwardE;
       #63: calculateAwardE2;
       #72: if y >  0 then dec(y);
       #75: if x >  0 then dec(x)
             else if y>0 then begin
              dec(y);
              x := 15;
             end;
       #77: if x < 15 then inc(x)
             else if y<7 then begin
              inc(y);
              x := 0;
             end;
       #80: if y <  7 then inc(y);
     end;
  end;
 until false;
end;

end.