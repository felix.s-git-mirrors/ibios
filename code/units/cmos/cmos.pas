unit cmos;
interface

  procedure cmosByteWrite (cAddr, cVal : byte);
  function  cmosByteRead  (cAddr : byte)        : byte;

  var       cmosSandboxArray : array [0..127] of byte;
  const     cmosSandboxUse   : boolean = false;

implementation

  procedure cmosByteWrite (cAddr, cVal : byte);
  begin
   if cmosSandboxUse then cmosSandboxArray[cAddr] := cVal
   else begin
    port[$0070] := cAddr;
    port[$0071] := cVal;
   end;
  end;

  function  cmosByteRead  (cAddr : byte)        : byte;
  begin
   if cmosSandboxUse then cmosByteRead := cmosSandboxArray[cAddr]
   else begin
    port[$0070] := cAddr;
    cmosByteRead := port[$0071];
   end;
  end;
end.