unit biosami;
{$G+}
interface
 uses apps,cmos;
 function amicrack(n : byte) : String20;

implementation

function test(val : byte) : boolean;
  var
    res : byte;
    i : byte;
begin
  res := 0;
  for i := 0 to 7 do begin
    res := res xor (val and 1);
    val := val shr 1;
  end;
  test := res<>0;
end;


{ this new implementation of getami is written by bluefish,
  with some aid of Christophe Grenier. Partially based on
  searchp/cmospwd and the "ami cracker (c) bugs"
}

function getami(mode, pos, len : byte; sc : boolean) : string20;
var al, ah, cl, i : byte;
    s : string20;
begin

  s := '';

  case mode of
    1:   al := cmosByteRead(pos-1);
    2:   al := cmosByteRead(pos-1) AND $F0;
    else al := $80;
  end;

  for i := pos to (pos + len - 1) do begin
     {loc1}
     ah := al;
     al := cmosByteRead(i);

     if al=0 then break; { null terminated string }

     {loc2}
     for cl := 0 to 255 do begin
        if (al=ah) then break;

	if (not test(al and $e1)) then begin
		{loc3}
		al := al shl 1;
	end else begin
		{loc3}
		al := al shl 1;
		al := al or 1;
	end;
     end;

     al := cl;
     if sc
     then s := s + scan2ascii(char(al))
     else s := s + char(al);

     al := cmosByteRead(i);
   end;
   getami := s;
end;


function amicrack(n : byte) : String20;
begin
 case n of
  1: amicrack := getami(2,$38,6,false);
  2: amicrack := getami(0,$38,6,true);
  3: amicrack := getami(1,$38,6,true);
  else begin
   writeLn ('Bad indata to AMI cracker: ',n);
   halt;
  end;
 end;

end;

end.

