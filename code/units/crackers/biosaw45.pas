{$G+}
unit biosaw45;

interface
uses apps,cmos,crt;

function  award450Crack(var s : openstring) : word;
procedure award450brute;
function  award450_fastAttack (attack : byte) : string8;
function  awardM460_CharDecode (encoded : byte) : char;
function  awardM460_Decode (AW_SEG, AW_BASE, AW_LEN : Word) : string8;

implementation

function AwardM460_CharDecode (encoded : byte) : char;
begin
  AwardM460_CharDecode:= char(
    (encoded SHL 5) OR
    (encoded SHR 5) OR
    (encoded AND b00011000) );
end;

function AwardM460_Decode (AW_SEG, AW_BASE, AW_LEN : Word) : string8;
var
 i : word;
 s : string8;
begin
 s := '';
 i := 0;
 while ( (i<AW_LEN) and ( MEM[ AW_SEG:AW_BASE+i] <> 0 ) ) do begin
  s := s + AwardM460_CharDecode( MEM[ AW_SEG:AW_BASE+i] );
  inc(i);
 end;
 AwardM460_Decode := s;
end;

const pwdperupdate = 4000;

var
    s : string;
    w2,
    crc : word;

function exp (a1, a2 : word) : word;
var i,res : word;
begin
 res := 1;
 for i := 1 to a2 do res := res*a1;
 exp := res;
end;

function award450Crack(var s : openstring) : word; assembler;
asm
 push ds
 push si
 xor ax,ax
 xor bx,bx
 xor cx,cx
 lds si, s
 lodsb
 mov cl,al
 @l1:
 rol bx,2
 lodsb
 add bx,ax
 loop @l1
 pop si
 pop ds
 mov ax,bx
end;

{$i bruter.pas}
{$i biosawb2.pas}



procedure smartAttack2 (len : byte);
var w : word;
    s1, s2 : string;
    n1, n2 : word;
    cnt : byte;
var a,b,c : byte;

begin
 s1:='';
 for b := 1 to len do s1:=s1+'a';
 cnt := 1;
 s := s1;
 for a := len downto cnt do begin
  for c := byte('b') to byte('z') do begin
   s2 := s;
   s2[a] := char(c);
   n1 := award450Crack(s1);
   n2 := award450Crack(s2);
   if (n2 <= w2) and (n2 >= n1) then begin
    s1 := s2;
    cnt := a;
   end;
  end;
 end;
 s := s1;
end;

procedure preTest (s : string);
begin
 if w2 = award450Crack(s) then begin
   write ('Pre-test match: ');
   textColor(lightred);
   if s = ''
   then writeLn ('no password [blank]')
   else writeLn (s);
   textcolor (lightgray);
 end;
end;

var
 b,c : word;
 l,lmax : longint;
 ch : char;
 lasty : byte;



procedure bruteforceSmartAward;
begin
 clrScr;
 writeLn('Eleventh Alliance''s Award BIOS 4.5X Bruteforcer'+rad);
 preTest ('');
 preTest ('589589');
 preTest ('condo');

 writeLn('Detecting best startup value... ');
 lmax := 1000;
 for b := 5 to 8 do begin
  smartAttack2(b);
  crc := award450Crack(s);
  l := (100*longint(w2)) div longint(crc);
  if l > 100
  then l := l - 100
  else l := 100 - l;
  if l < lmax then begin
   lmax := l;
   c := b;
  end;
 end;
 smartAttack2 (c);
 gotoXY(1,wherey);
 writeLn ('Bruteforcing ',pwdperupdate,' passwords per screen update!');
 b := 0;
 lasty := wherey;
 window(57,1,80,25);
 repeat
  bruteforceAsm (s, 'a', 'z');
  crc := award450Crack(s);
  if b > pwdperupdate then begin
    b := 0;
    writeLn(s);
    if keypressed then begin
     if readkey = #27 then begin
      window(1,1,80,25);
      clrScr;
      exit;
     end;
    end;
  end else inc(b);
 until crc = w2;
 window(1,lasty+1,53,25);
 write('Valid password -> ');
 textColor(lightred);
 writeLn (s+rad);
 textcolor (lightgray);

 writeLn ('Please note that since Award uses a hash routine,'+rad+
          'a very simple form of none reversable encryptions,'+rad+
          'we can only find valid password(s), not guess what'+rad+
          'actually has been typed in.');
 writeLn ('(c) 1997-98 Bluefish [11A]');
 if readkey =#0 then readkey;;
 window(1,1,80,25);
end;

FUNCTION PWDIGITCrack(chk:word):STRING8;
{ PWDIGIT.PAS -- Jan Stohner }
Var  phrase : STRING8;
     i      : INTEGER;
     help   : WORD;
BEGIN
  help := chk;
  phrase := '00000000';
  FOR i:=8 DOWNTO 1 DO
  BEGIN
    phrase[i] := chr(ord(phrase[i]) OR (help AND 3));
    help := help SHR 2;
  END;

  IF chk<48 THEN phrase[1] := chr(ord(phrase[1]) OR 4);
  IF (chk>=48) AND (chk<52) THEN phrase[8] := chr(ord(phrase[8]) + 1);
  IF (chk>=52) AND (chk<256) THEN phrase[8] := chr(ord(phrase[8]) OR 4);
  IF (chk>=256) AND (chk<1024) THEN phrase[7] := chr(ord(phrase[7]) OR 4);
PWDIGITCrack:=phrase;
END;



procedure award450brute;
begin
 textAttr := lightgray;
 clrScr;
 writeLn (rad+rad);
 textcolor(cyan);
 write ('Press'); textcolor(lightcyan); write ('  1  ');
 textcolor(cyan); writeLn ('in order to crack Supervisor/System passwords');
 write ('Press'); textcolor(lightcyan); write ('  2  ');
 textcolor(cyan); writeLn ('in order to crack User passwords');
 write ('Press'); textcolor(lightcyan); write ('  3  ');
 textcolor(cyan); writeLn ('in order to crack User passwords (other versions)');
 write ('Press'); textcolor(lightcyan); write ('  4  ');
 textcolor(cyan); writeLn ('in order to crack 4.50 masterpasswords');
 write ('Press'); textcolor(lightcyan); write ('  5  ');
 textcolor(cyan); writeLn ('in order to crack 4.51/4.60 masterpasswords');
 write ('Press'); textcolor(lightcyan); write ('  6  ');
 textcolor(cyan); writeLn ('in order to enter checksum manually');
 write ('Press'); textcolor(lightcyan); write (' Esc ');
 textcolor(cyan); writeLn ('in order to quit');
 textcolor(lightGray);
 while true do begin
  case readkey of
   #0: readkey;
   #27 : begin
          clrScr;
          exit;
         end;
   '1' : begin
          w2 := cmosByteRead($1D);
          w2 := w2*256+cmosByteRead($1C);
          break;
         end;
   '2' : begin
          w2 := cmosByteRead($4E);
          w2 := w2*256+cmosByteRead($4D);
          break;
         end;
   '3' : begin
          w2 := cmosByteRead($61);
          w2 := w2*256+cmosByteRead($60);
          break;
         end;
   '4' : begin
          w2 := memw[$F000:$EC60];
          break;
         end;
   '5' : begin
          writeLn;
          textcolor(green);
          write   ('The password is: ');
          textcolor(lightgreen);
          writeLn(AwardM460_Decode($F000,$EC60,8));
          if readkey = #0 then readkey;
          exit;
         end;
   '6' : begin
          textcolor(green);
          write(rad+'Enter checksum in decimal form: ');
          textcolor(lightgreen);
          readLn(w2);
          break;
         end;
  end;
 end;
 clrScr;
 writeLn (rad+rad);

 textcolor(cyan);
 write ('Press'); textcolor(lightcyan); write ('  1  ');
 textcolor(cyan); writeLn ('use Jan Stohner''s PWDigit (very fast!)');

 write ('Press'); textcolor(lightcyan); write ('  2  ');
 textcolor(cyan); writeLn ('(not very) intelligent bruteforce');

 write ('Press'); textcolor(lightcyan); write ('  3  ');
 textcolor(cyan); writeLn ('all passwords bruteforce');

 write ('Press'); textcolor(lightcyan); write ('  4  ');
 textcolor(cyan); writeLn ('simply print checksum (uncracked)');

 write ('Press'); textcolor(lightcyan); write (' Esc ');
 textcolor(cyan); writeLn ('in order to quit');

 textcolor(lightGray);
 while true do begin
  case readkey of
   #0: readkey;
   #27 : begin
          clrScr;
          exit;
         end;
   '2' : begin
          bruteforceSmartAward;
          break;
         end;
   '3' : begin
          bruteforceAllAward(w2);
          turnCursor(off);
          break;
         end;
   '1' : begin
          writeLn;
          textcolor(green);
          write   ('The password is: ');
          textcolor(lightgreen);
          writeLn (PWDIGITCrack(w2));
          if readkey = #0 then readkey;
          break;
         end;
   '4' : begin
          writeLn;
          textcolor(green);
          write   ('The checksum (not the password!) is: ');
          textcolor(lightgreen);
          writeLn (w2);
          if readkey = #0 then readkey;
          break;
         end;
  end;
 end;
end;

function  award450_fastAttack (attack : byte) : string8;
var s: string;
begin
 case attack of
   1 : begin
          w2 := cmosByteRead($1D);
          w2 := w2*256+cmosByteRead($1C);
         end;
  2 : begin
          w2 := cmosByteRead($4E);
          w2 := w2*256+cmosByteRead($4D);
         end;
  3 : begin
          w2 := memw[$F000:$EC60];
         end;
  4 : begin
          readLn (w2);
         end;
  5 : begin
          w2 := cmosByteRead($61);
          w2 := w2*256+cmosByteRead($60);
         end;
 end;
 {s := '';
 repeat
  bruteforceAsm (s, '0', '9');
 until w2 = award450Crack(s);}
 award450_fastAttack := PWDIGITCrack(w2);
end;
end.
