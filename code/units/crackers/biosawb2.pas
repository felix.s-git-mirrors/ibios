
const apwd = 'AWARD.PWD';
var bxx, byy, ymax: byte; color : boolean; hits: longint; savetofile : boolean;

procedure writeLnApwd(s : string);
var f : text;
begin
 if savetofile then begin
  assign(f, apwd);
  if fileexists(apwd) then append(f) else rewrite(f);
  writeln(f, s);
  close(f);
 end;
end;


procedure displayhit (s : string8);
begin
 inc(hits);
 inc(byy);
 if byy >= ymax then begin
  byy := 4;
  inc(bxx);
  if (bxx*10) > 80 then begin
    bxx := 1;
    if color = true then color := false else color := true;
  end;
 end;
 if color = true then textColor (white) else textColor (yellow);
 gotoXY ((bxx-1)*10+1,byy);
 write (s);
 writeLnApwd(s);
end;


procedure bruteforceAllAward(w : word);

var
 brute : bruteforcer;
 combos : word;
 fc : byte;
 lm : word;
begin

 clrScr;
 writeLn (rad+rad);
 textcolor(cyan);
 write ('Press'); textcolor(lightcyan); write ('  1  ');
 textcolor(cyan); writeLn ('bruteforce with a-z');
 write ('Press'); textcolor(lightcyan); write ('  2  ');
 textcolor(cyan); writeLn ('bruteforce with 0-9');
 write ('Press'); textcolor(lightcyan); write ('  3  ');
 textcolor(cyan); writeLn ('bruteforce with a-z, 0-9');
{ write ('Press'); textcolor(lightcyan); write ('  4  ');
 textcolor(cyan); writeLn ('bruteforce with a-z, A-Z, 0-9');}
 write ('Press'); textcolor(lightcyan); write (' Esc ');
 textcolor(cyan); writeLn ('in order to quit');
 textcolor(lightGray);
with brute do begin
 while true do begin
  case readkey of
   #0: readkey;
   #27 : begin
          clrScr;
          exit;
         end;
   '1' : begin
          charset := 'abcdefghijklmnopqrstuvxyz';
          break;
         end;
   '2' : begin
          charset := '0123456789';
          break;
         end;
   '3' : begin
          charset := 'abcdefghijklmnopqrstuvxyz0123456789';
          break;
         end;
{   '4' : begin
          charset := 'abcdefghijklmnopqrstuvzyxABCDEFGHIJKLMNOPQRSTUVXYZ0123456789';
          break;
         end;}

  end;
 end;
 init(1);
end;
 clrScr;
 textcolor(cyan);
 write ('Press'); textcolor(lightcyan); write ('  1  ');
 textcolor(cyan); writeLn ('save to file '+apwd);
 write ('Press'); textcolor(lightcyan); write ('  2  ');
 textcolor(cyan); writeLn ('don''t save to file');

 while true do begin
  case readkey of
   #0: readkey;
   #27 : begin
          clrScr;
          exit;
         end;
   '1' : begin
          savetofile := true;
          writeLnApwd('New bruteforce (checksum '+abc(w,0)+')');
          break;
         end;
   '2' : begin
          savetofile := false;
          break;
         end;
  end;
 end;

 lm := lastmode;
 textmode(C80 + Font8x8);
 gotoxy(1,25);
 gotoxy(1,43);
 gotoxy(1,50);
 ymax := wherey;
 bxx  := 1;
 byy  := 3;
 hits := 0;
 clrScr;

 combos := 0;
 fc := 0;
 repeat
  gotoxy (1,1);
  write (brute.Result+' ('+abc(hits,0)+' hits)');
  if keypressed then begin
   if readkey = #27 then begin
    textmode(lm);
    exit;
   end else
    while keypressed do readkey;
  end;
  for combos := 1 to 10011 do begin

   if w = award450Crack(brute.result) then begin
    displayhit(brute.result);
   end;
   brute.brute;
  end;
 until length(brute.result)>8;
 writeLn(rad+'---> Enter to terminate');
 textmode(lm);
end;
    {gotoxy (1,wherey);
    writeLn('HIT: ',brute.result);
    inc(fc);
    if fc = ymax-3 then begin
     fc := 0;
     write('---> Any key to continue');
     if readkey = #0 then readkey;
     writeLn;
    end;}
