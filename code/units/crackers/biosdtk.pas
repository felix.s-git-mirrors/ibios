unit biosdtk;

interface
 uses apps,cmos;
 function getDTK427pwd (fc,chars:byte) : string;

implementation

function getDTK427pwd (fc,chars:byte) : string;
var
 s      : string;
 pos, b : byte;
 doExit : boolean;

begin
 s := '';
 doExit := false;
 for pos := 0 to chars-1 do if not doExit then begin
  case pos of
   0 : b := cmosByteRead (fc+0) shr 2;
   1 : b := cmosByteRead (fc+0) shl 4 + cmosByteRead (fc+1) shr 4;
   2 : b := (cmosByteRead (fc+1) shl 2) and b00111100 + cmosByteRead (fc+2) shr 6;
   3 : b := cmosByteRead (fc+2);
   4 : b := cmosByteRead (fc+3) shr 2;
   5 : b := cmosByteRead (fc+3) shl 4 + cmosByteRead (fc+4) shr 4;
  end;
  b := b and b00111111;
  case b of
       00: doExit := true;
   01..10: s := s+char(b-1 +byte('0'));
   11..63: s := s+char(b-11+byte('a'));
  end;
 end;
 getDTK427pwd := s;
end;

end.