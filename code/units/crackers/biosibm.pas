unit BIOSIBM;
interface
uses biosami,apps,cmos;
const
 ibm1pwds = $48;
 ibm1pwdl = 6;

function scanDEcode(start, len : byte) : string8;
 { general cracker for pwds saved in scancodes }

function ibm1decode : string8; { cracker for IBM SurePath BGOSV1E }

implementation

function IBM1getCMOS ( b : byte ) : char;
 { reads from CMOS and convert scancode to ascii. uses BIOSAMI }
var c : char;
begin
 c := char(cmosByteRead(b));
 IBM1getCMOS := scan2ascii(c);
end;


function ibm1decode : string8;
var
 s : string;
begin
 s := scanDEcode (ibm1pwds, ibm1pwdl);
 ibm1decode := s;
end;

function scanDEcode (start, len : byte) : string8;
var
 b : byte;
 s : string8;
 c : char;
begin
 s[0] := #0;
 for b:= start to start+len-1 do begin
  c := IBM1getCMOS(b);
  if byte(c) >= 32 then s := s+c;
 end;
 scanDEcode := s;
end;

end.