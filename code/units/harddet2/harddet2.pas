unit HardDet2;

{$G+}
interface
 uses dos,crt,xms,timetask,ipx1,apps;

 procedure harddet2init (verbose : boolean);
 {must be executed else unit won't work!}

 var bustype : string[10];

 function IPX_Loaded         : Boolean;
 function Netbios_Installed  : Boolean;
 function NetShell_Installed : Boolean;

 function stationno : byte;
 procedure GetUserName( var UserName : string );

 function ipxNode    : string12;
 function ipxNetwork : string8;

 procedure displayTestInfo;


var
   Network      : S4BYTE;
   Node         : S6BYTE;
   IpxSetupRes,
   IpxINWAdressRes : byte;

implementation

var   Regs : registers ;
      ReplyBuffer : array[1..40] of char ;


function IPX_Loaded:boolean;
begin
   Regs.AX := $7A00 ;
   intr($2F,Regs) ;
   IPX_Loaded := (Regs.AL = $FF)
end;

function Netbios_Installed:Boolean;
begin
 Regs.AH := $35; (* DOS function that checks an interrupt vector *)
 Regs.AL := $5C; (* Interrupt vector to be checked *)
 NetBios_Installed := True;
 msdos(Regs) ;
 if ((Regs.ES = 0) or (Regs.ES = $F000))
   then  NetBios_Installed := False
end;


function NetShell_Installed:Boolean;
begin
   with Regs do begin
      AH := $EA ;
      AL := 1 ;
      BX := 0 ;
      ES := seg(ReplyBuffer) ;
      DI := ofs(ReplyBuffer) ;
   end ; (* with do begin *)
   msdos(regs) ;
   NetShell_Installed := (Regs.BX = 0)
end;
 procedure cputest;
 begin
  if (Test8086 = 2) then         { RTL check stops at 2 = 386}
  asm
             inc    Test8086     { 3 = 386, for consistency }
    { Do we have a 386 or a 486? }
    { Does pushf/popf preserve the Alignment Check bit? (386=no, 486=yes) }
             mov    bx, sp       { save current stack pointer }
             and    sp, not 3    { align stack to avoid AC fault }
    db $66;  pushf
    db $66;  pop    ax
    db $66;  mov    cx, ax
    db $66, $35; dd $40000       { xor AC bit in EFLAGS }
    db $66;  push   ax
    db $66;  popf
    db $66;  pushf
    db $66;  pop    ax
    db $66;  xor    ax, cx       { Is AC bit toggled? }
             je @@1              { if not, we have a 386 }
             and    sp, not 3    { align stack to avoid AC fault }
    db $66;  push   cx
    db $66;  popf                { restore original AC bit }
             mov    sp, bx       { restore original stack pointer }
             mov  Test8086, 4    { we know we have at least a 486 }

    { Do we have a 486 or a Pentium? }
    { Does pushf/popf preserve the CPUID bit? (486=no, P5=yes) }
    db $66;  mov    ax, cx       { get original EFLAGS}
    db $66, $35; dd $200000      { XOR id bit in flags}
    db $66;  push   ax
    db $66;  popf
    db $66;  pushf
    db $66;  pop    ax
    db $66;  xor    ax, cx      { Is CPUID bit toggled? }
             je @@1             { if not, we have a 486 }
    db $66;  xor    ax, ax
    db $f,$a2                   { CPUID, AX = 0 (get CPUID caps) }
    db $66;  cmp    ax, 1
             jl @@1             { if < 1, then exit }
    db $66;  xor    ax, ax
    db $66;  inc    ax
    db $f,$a2                   { CPUID, AX = 1 (get CPU info)   }
             and    ax, $f00    { mask out all but family id }
             shr    ax, 8
             mov    Test8086, al      { Pentium family = 5 }
   @@1:
  end;
 end;

procedure getbustype;
var
  works         : boolean;
  data_seg      : word;
  data_ofs      : word;
  test          : string[4];


begin
  bustype:='';
  works:=false;

  if not works then                     { EISA }
  begin
    test:='EISA';
    test[2]:=chr(mem[$f000:$ffd9]);
    test[1]:=chr(mem[$f000:$ffda]);
    test[4]:=chr(mem[$f000:$ffdb]);
    test[3]:=chr(mem[$f000:$ffdc]);
    if test='EISA'  then
    begin
      works:=true;
      bustype:='EISA';
    end;
  end;

  if not works then                     { MCA }
  begin
    asm
      mov ah,0c0h
      int 15h
      cmp ah,0
      jnz @nope

      mov works,true

      mov data_seg,es
      mov data_ofs,bx

    @nope:
    end;
    if works then if (mem[data_seg:data_ofs+5] and 2)=2 then bustype:='MCA'
else works:=false;  end;

  if not works then                     { PCI }
  begin
    asm
      mov ax,$b101
      int $1a
      cmp ah,00
      jne @nope
      mov works,true
    @nope:
    end;
    if works then bustype:='ISA/PCI';
  end;
  if not works then bustype:='ISA';     { ISA ? }
end;

Function stationno : byte;
var
  B : byte;
  Regs : Registers;
begin
  With Regs do
  begin
    ah := $DC;
    ds := 0;
    si := 0;
  end;
  MsDos( Regs ); {INT $21,ah=dh}
  b := Regs.al;
  stationno := b;
end;

procedure GetUserName( var UserName : string );

var
  Request : record                     { Request buffer for "Get Conn Info" }
    Len  : Word;                       { Buffer length - 2                  }
    Func : Byte;                       { Subfunction number ( = $16 )       }
    Conn : Byte                        { Connection number to be researched }
  end;

  Reply    : record                    { Reply buffer for "Get Conn Info"   }
    Len    : Word;                     { Buffer length - 2                  }
    ID     : Longint;                  { Object ID (hi-lo order)            }
    Obj    : Word;                     { Object type (hi-lo order again)    }
    Name   : array[ 1..48 ] of Byte;   { Object name as ASCII string        }
    Time   : array[ 1.. 7 ] of Byte;   { Y, M, D, Hr, Min, Sec, DOW         }
                                       { Y < 80 is in the next century      }
                                       { DOW = 0 -> 6, Sunday -> Saturday   }
    Filler : Byte                      { Call screws up without this!       }
  end;

  Regs   : Registers;
  W      : Word;

begin
  Regs.AX := $DC00;                    { "Get Connection Number"            }
  MsDos( Regs );
                                       { "Get Connection Information"       }

  with Request do                      { Initialize request buffer:         }
  begin
    Len := 2;                                    { Buffer length,           }
    Func := $16;                                 { API function,            }
    Conn := Regs.AL                    { Returned in previous call!         }
  end;

  Reply.Len := SizeOf( Reply ) - 2;    { Initialize reply buffer length     }

  with Regs do
  begin
    AH := $E3;                         { Connection Services API call       }
    DS := Seg( Request );              { Location of request buffer         }
    SI := Ofs( Request );
    ES := Seg( Reply );                { Location of reply buffer           }
    DI := Ofs( Reply );
    MsDos( Regs )
  end;

  if ( Regs.AL = 0 )                        { Success code returned in AL   }
       and ( Hi( Reply.Obj ) = 1 )          { Obj of 1 is a user,           }
       and ( Lo( Reply.Obj ) = 0 ) then     {   stored Hi-Lo                }
    with Reply do
    begin
      Move( Name, UserName[ 1 ], 48 );           { Convert ASCIIZ to string }
      UserName[ 0 ] := #48;
      W := 1;
      while ( UserName[ W ] <> #0 )
            and ( W < 48 ) do
        Inc( W );
      UserName[ 0 ] := Char( W - 1 )
    end
  else
    UserName := ''
end;

function ipxNode : string12;
var b : byte; s : string12;
begin
 s := '';
 for b := 1 to 6 do s := s + hexbyte(Node[b]);
 ipxNode := s;
end;

function ipxNetwork : string8;
var b : byte; s : string8;
begin
 s := '';
 for b := 1 to 4 do s := s + hexbyte(Network[b]);
 ipxNetwork := s;
end;

procedure displayTestInfo;

const
 x2 = 30;
 y1 = 2;
type string3 = string[3];


function YN (b : boolean) : string3;
begin
 if b = Yes then YN:=('Yes') else YN:=('No');
end;

var
 w : word; s : string;
 tattr : byte;

procedure cWriteLn (color : byte; s : string);
var b : byte;
begin
 b := textattr;
 textcolor(color);
 write(s);
 textattr := b;
 writeLn;
end;

begin
 tattr := textattr;
 clrScr;
 gotoxy (1,y1);
 cWriteLn(darkgray,'Hardware');
 write ('CPU Type:           ');
 case test8086 of
  0 : writeLn ('8088');
  1 : writeLn ('80286');
  2 : writeLn ('80386');
  3 : writeLn ('80386');
  4 : writeLn ('80486');
  5 : writeLn ('Pentium');
  6 : writeLn ('Pentium Pro');
  7 : writeLn ('Pentium II');
  else writeLn ('Unknown processor');
 end;
 writeLn ('Bus Type:           ',bustype);
 cWriteLn(darkgray,'Memory');
 if maxAvail = memAvail then
 writeLn ('Important notice:   Your memory is optimized.')
 else
 writeLn ('Largest free block: ', maxAvail div k,'k');
 writeLn ('Free Con. 640k:     ', memAvail div k,'k');
 writeLn ('Free XMS memory:    ', XMS_totalfree div k, 'k');


 cWriteLn(darkgray,'Software');
 writeLn ('MS-DOS Version:     ',lo(dosVersion),'.',hi(dosVersion));
 write   ('Multi-Tasking:      ');
 case task.os of
  1 : writeLn ('Windows');
  2 : writeLn ('OS/2');
  3 : writeLn ('DesqView');
  4 : writeLn ('TopView');
  else writeLn ('None');
 end;
 if task.os <> 0
 then writeLn ('Version:            ',hi(task.Version), '.', lo(task.Version));



 cWriteLn(darkgray,'Network, IPX');
 writeLn ('IPX Supported:      ',YN(ipx_loaded));
 if ipx_loaded = Yes then begin
  writeLn ('NetBIOS Supported:  ',YN(netBios_installed));
{  writeLn ('NetShell Supported: ',YN(netShell_installed));}
  if ipxSetupRes <> 0
  then begin
   write   ('IPX Initialization: ');
   cWriteLn(lightred,'Failure: '+abc(ipxSetupRes,1))
  end
  else begin
   if ipxINWAdressRes <> 0
   then begin
    write   ('IPX Client-ID test: ');
    cWriteLn(lightred,'Failure: '+abc(ipxINWAdressRes,1))
   end
   else begin
    writeLn ('IPX Network:        ',IpxNetwork);
    writeLn ('IPX Node:           ',IpxNode);
   end;
  end;
  writeLn ('Novell Workstation: ',stationno);
  write   ('Novell Login name:  ');
  getUserName(s);
  if s = ''
  then cWriteLn(lightred,'OFFLINE')
  else writeLn (s);
 end; { ---------------------------------------------- [ Ipx Supported? ] }

 while not keypressed do timeslice;
 while keypressed do readkey;
 textattr := tattr;
end;

procedure harddet2init (verbose : boolean);
begin
 if verbose then writeLn ('[harddet] Detecting CPU...');
 cputest;

 if verbose then writeLn ('[harddet] Detecting Bus...');
 getbustype;

 if verbose then writeLn ('[harddet] Detecting memory...');
 xmsInit;

 if not timetask.initilized then begin
   if verbose then writeLn ('[harddet] Detecting OS...');
   timetask.init;
 end;

 if verbose then writeLn ('[harddet] Detecting IPX Networking...');
 IpxSetupRes     := IPX_SETUP;
 if IpxSetupRes = 0
 then IpxINWAdressRes := IPX_Internetwork_Address(Network,Node);

end;

end.
