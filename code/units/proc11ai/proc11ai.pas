unit proc11ai;
interface
 uses apps;

 procedure proc11aProtect (fileName : string80);
 function  proc11aCheck   (fileName : string80) : boolean;
 function  proc11aCheckAV (fileName : string80) : boolean;

 var
  proc11aEOF,
  proc11aLEN,
  proc11aSIZE : longint;


implementation

var
 rCnt  : integer;
 rArr  : array [1..255] of byte;
 HASH  : file;

 hash1, hash1o,
 hash2, hash2o,
 hash3, hash3o : LongInt;

const mld = maxLongInt div 2;

procedure hashProc;
var b : byte; h1,h2 : longint;
begin
 h1 := 1;
 h2 := 1;
 for b:= 1 to rCnt do begin
   h1 := rArr[b] xor (b*32) xor (h1*16);
   h2 := rArr[b] + h2;
 end;
 hash1 := hash1 xor h1;
 hash2 := hash2 + h2;
 if hash2 > mld then hash2 := hash2-mld;
end;

procedure getNewHash;
begin
 hash1 := 0;
 hash2 := 0;
 hash3 := fileSize(HASH);
 seek (HASH, $70);
 repeat
  blockRead (HASH, rArr, sizeOf(rArr), rCnt);
  if (filePos(HASH) > hash3o) then rCnt := longInt(rCnt) + hash3o - filePos(HASH);
  hashProc;
 until (rCnt < sizeOf(rArr));
end;

procedure getOldHash;
begin
 seek (HASH, $50);
 blockRead (HASH, hash1o, sizeOf(hash1o));
 blockRead (HASH, hash2o, sizeOf(hash2o));
 blockRead (HASH, hash3o, sizeOf(hash3o));
 proc11aEOF  := hash3o;
 proc11aSIZE := fileSize(HASH);
 proc11aLEN  := fileSize(HASH)-hash3o;
end;

procedure writeNewHash;
begin
 seek (HASH, $50);
 hash3 := fileSize(hash);
 blockWrite (HASH, hash1,  sizeOf(hash1));
 blockWrite (HASH, hash2,  sizeOf(hash2));
 blockWrite (HASH, hash3,  sizeOf(hash3));
end;

function proc11aCheck (fileName : string80) : boolean;
begin
 {$I-}
 assign (HASH, fileName);
 reset  (HASH, 1);
 getOldHash;
 getNewHash;
 close(HASH);
 proc11aCheck := true;
 if hash1 <> hash1o then proc11aCheck := false;
 if hash2 <> hash2o then proc11aCheck := false;
 if hash3 <  hash3o then proc11aCheck := false;
 {$I+}
 {
 writeLn(hash1);
 writeLn(hash1o);
 writeLn(hash1 <> hash1o);
 writeLn;
 writeLn(hash2);
 writeLn(hash2o);
 writeLn(hash2 <> hash2o);
 writeLn;
 writeLn(hash3);
 writeLn(hash3o);
 writeLn(hash3 < hash3o);
 writeLn;
 }
end;

function proc11aCheckAV (fileName : string80) : boolean;
var b : boolean;
begin
 b := proc11aCheck(fileName);
 if b then b := (proc11aSIZE = proc11aEOF);
 proc11aCheckAV := b;
end;

procedure proc11aProtect (fileName : string80);
begin
 assign (HASH, fileName);
 reset  (HASH, 1);
 hash3o := fileSize(HASH);
 getNewHash;
 writeNewHash;
 close(HASH);
end;

end.


