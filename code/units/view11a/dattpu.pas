unit dattpu;
{$B+,S+}
interface
  uses apps;

  type datinfo = record
   dh1, dh2, dl : longint;
   reserverat : array [1..10] of byte;
  end;
  var
    dat : datinfo;

  procedure dat_seek (no : byte) ;
  procedure dat_createDat (source, dest : string80);
  function  dat_datIsOkay : boolean;
  function  dat_allDatIsOkay(screenOut : boolean) : boolean;
  var
   dat_fv : file;
   noDats : byte;

implementation
var
 hash1, hash2 : longint;
 bArr : array [1..k] of byte;
 bCnt : word;
const mld = maxLongInt div 2;

procedure hashProc;
var w:word; h1,h2 : longint;
begin
 h1 := 1;
 h2 := 1;
 for w := 1 to bCnt do begin
   h1 := bArr[w] xor (w*32) xor (h1*16);
   h2 := bArr[w] + h2;
 end;
 hash1 := hash1 xor h1;
 hash2 := hash2 + h2;
 if hash2 > mld then hash2 := hash2-mld;
end;


procedure dat_createDat (source, dest : string80);
var fs, fd : file; startp : longint; b : byte;
begin
 if fileExists(source) then begin
  assign (fs, source);
  reset (fs, 1);
  assign (fd, dest);
  if not fileExists(dest)
  then begin
   rewrite (fd, 1);
   startp := 0;
  end else begin
   reset (fd, 1);
   startp := fileSize(fd);
   seek  (fd, startp);
   blockWrite (fd, dat, sizeOf(dat));
  end;
  hash1 := 0;
  hash2 := 0;
  repeat
   blockRead  (fs, bArr, sizeOf(bArr), bCnt);
   hashProc;
   blockWrite (fd, bArr, bCnt, bCnt);
  until (bCnt < sizeOf(bArr));
  seek  (fd, startp);
  dat.dh1 := hash1;
  dat.dh2 := hash2;
  dat.dl  := fileSize (fs);
  for b := 1 to sizeOf(dat.reserverat) do dat.reserverat[b] := 0;
  blockWrite (fd, dat, sizeOf(dat));
  close(fd);
  close(fs);
 end;
end;

function dat_datIsOkay : boolean;
var b : byte; startp, rePos : longint;
begin
 dat_datIsOkay := false;
 hash1 := 0;
 hash2 := 0;
 blockRead (dat_fv, dat, sizeOf(dat));
 startp := filePos(dat_fv);
 repeat
  blockRead (dat_fv, bArr, sizeOf(bArr), bCnt);
  if (filePos(dat_fv) > startP+dat.dl) then bCnt := longInt(bCnt) + startP+dat.dl - filePos(dat_fv);
  hashProc;
 until (bCnt < sizeOf(bArr));
 if (dat.dh1 = hash1) and (dat.dh2 = hash2) then dat_datIsOkay := true;
 seek (dat_fv, startP+dat.dl);
end;

function dat_allDatIsOkay(screenOut : boolean) : boolean;
var bo : boolean;
begin
 noDats := 0;
 bo := true;
 writeLn (filePos(dat_fv), fileSize(dat_fv));
 while filePos(dat_fv) < fileSize(dat_fv) do begin
  inc(noDats);
  if not dat_datIsOkay then begin
   bo := false;
   if screenOut then writeLn('DAT ',noDats, ' is damaged');
  end else if screenOut then writeLn('DAT ',noDats, ' is OKAY');
 end;
 dat_allDatIsOkay := bo;
end;

procedure dat_seek (no : byte) ;
var b : byte;
begin
 blockRead (dat_fv, dat, sizeOf(dat));
 for b := 1 to no do begin
   seek (dat_fv, filePos(dat_fv)+dat.dl);
   blockRead (dat_fv, dat, sizeOf(dat));
 end;
end;


end.