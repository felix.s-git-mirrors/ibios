unit View11A2;
interface
uses crt,apps,dos,View11AL,datTPU,{proc11ai,}timetask,strings;
type
 charSet = record
  hline,  vline,
  lSplit, rSplit,
  luTurn, ruTurn,
  ldTurn, rdTurn : char;
  color : array[1..3] of byte;
 end;
type
 arrayofbyte = array [1..65535] of byte;
 parrayofbyte = ^arrayofbyte;

const chStd : charSet =(
hline: '�';  vline: '�';
lSplit: '�'; rSplit: '�';
luTurn: '�'; RuTurn: '�';
ldTurn: '�'; RdTurn: '�';
color: (darkGray,LightGray,White));
const
menuInfo  = '11th Menu O.2';
viewerID  = '11th View O.2';
website   = 'http://www.11a.nu/';
noFields  = 5;
maxEntries= 10;

var
 fieldsUsage : array [1..noFields] of (f_menu, f_text, f_sleep);

procedure viewerMainRoutine;

procedure viewerBeep;
procedure viewerByteArray2Array (p : array of byte; len : word);
procedure viewerPointer2Array (p : parrayofbyte; len : word);
procedure viewerFile2Array (fileName : string80);
procedure viewerDatFile2Array (fileName : string80; no : byte; startOffset : longInt);
procedure viewerPrint ( media : string );
procedure viewerScrUpdate;
procedure viewerInit (cs : charSet);


type frame = record
     x1, y1,
     x2, y2 : byte;
end;


type
 PTRmenuEntryArray = ^menuEntryArray;
 menuEntry = record
  name  : string20;
  txt   : {string100} pchar;
  sub   : PTRmenuEntryArray;
  isExe : boolean;
  proc  : procedure;
 end;
 menuEntryArray    = array [1..maxEntries] of menuEntry;
 menuField = record
  frm      : frame;
  selected : byte;
  entry    : menuEntryArray;
 end;
 menuFieldsR = record
  cNotF,
  cNotB,
  cSelB,
  cSelF,
  currentF : byte;
  menuF : array [1..noFields] of menuField;
 end;
var
  mf  : menuFieldsR;
empty : menuEntryArray;




procedure makeFrame2(f : frame; cs : charSet);
procedure txtBox (x,y : byte; txtColor : byte; txt : String60; cs : CharSet);
procedure verticalLine (x1,y,x2, color : byte; cs : charSet);
function  getErrorMsg (errCode : integer) : string;
procedure displayField (fieldno : byte);
procedure programBAR (y,pColor, mColor : byte; txt : string60; cs : charSet; dispMenuInfo : boolean);

function  TSR11aINT (INT : byte; TSR : word) : boolean;


const
 viewerLineMax = 6000;
 viewerResetMode : boolean = false;
 viewerHexCnt    : boolean = true;
var
 viewerLineCurrent : integer;
 viewerLineCount   : integer;
 viewerLinePtr     : TPList;
 viewerYmax        : byte;
 viewerIOR         : Integer;
 viewerInfF,
 viewerInfA,
 viewerInfS        : string40;
 viewerLastMode    : byte;
 viewerScrTxt,
 viewerScrCnt,
 viewerPrinterFrame,
 viewerInfoFrame   : frame;
 viewerCntColor,
 viewerTextColor   : byte;



implementation


var
 r : registers;
function TSR11aINT (INT : byte; TSR : word) : boolean;
begin
 r.ax := $43EA;
 r.bx := $011A;
 r.cx := TSR;
 intr (INT, r);
 TSR11aINT := (r.cx = r.bx);
end;

procedure TSRInfoSeek;
var x, y, x2, y2 : byte; found : boolean; c : char; w : word;
var intFrame1, intFrame2 : frame;
begin
 with intFrame2 do begin
  x1 := 10;
  x2 := 80-15;
  y1 := 6;
  y2 := y1+2;
 end;
 with intFrame1 do begin
  x1 := intFrame2.x1;
  x2 := intFrame2.x2;
  y1 := intFrame2.y2+3;
  y2 := y1+10;
 end;

 makeFrame2(intFrame1,chStd);
 makeFrame2(intFrame2,chStd);
 textColor (lightgray);
 found := false;
 with intFrame2 do begin
  window (x1+3,y1,x2,y2);
 end;
 gotoXY (1, whereY+2);
 write('INT21: Analysing ... ');
 x := whereX;
 y := whereY;
 with intFrame1 do begin
  window (x1+3,y1,x2,y2);
  gotoXY (1,y2-y1);
 end;
 writeLn;
 for w:= 0 to $11A-1 do begin
  if not keypressed then begin
   textcolor(lightgray); write ('INT21: TSR Application ');
   textcolor(lightcyan); write (hexword(w));
   textcolor(lightgray); write (' ...');
   gotoXY(whereX-3,whereY);
   if TSR11aINT($21,w) then begin
    found := true;
    write('detected');
    case w of
      $001: write (': Getit EAX');
      $002: write (': EaTimer');
      $003: write (': Stealth-X');
      $004: write (': Getit SDW');
      $005: write (': S-Shade');
      $006: write (': RaHacker');
      $007: write (': F-Shade');
      $008: write (': Getit SSH');
      $11A: write (': Internal echo');
     else write('.');
    end;
    if r.cx <> $11A then write (' ',r.ch,'.',abc(r.cl,2));
    writeLn;
   end else gotoXY(1,whereY);
  end;
 end;
 x2 := whereX;
 y2 := whereY;
 with intFrame2 do begin
  window (x1+3,y1,x2,y2);
 end;
 gotoXY(x,y);
 if found
 then writeLn('TSRs detected!')
 else writeLn('No TSRs are hooked...');
 found := false;
 write('INT16: Analysing ... ');
 x := whereX;
 y := whereY;
 with intFrame1 do begin
  window (x1+3,y1,x2,y2);
  gotoXY (1,y2-y1);
 end;
 gotoXY(x2,y2);
 writeLn;
 for w:= 0 to $11A-1 do begin
  if not keypressed then begin
   textcolor(lightgray); write ('INT16: TSR Application ');
   textcolor(lightcyan); write (hexword(w));
   textcolor(lightgray); write (' ...');
   gotoXY(whereX-3,whereY);
   if TSR11aINT($16,w) then begin
    found := true;
    write('detected');
    case w of
      $001: write (': Slicer');
     else write('.');
    end;
    if r.cx <> $11A then write (' ',r.ch,'.',abc(r.cl,2));
    writeLn;
   end else gotoXY(1,whereY);
  end;
 end;
 with intFrame2 do begin
  window (x1+3,y1,x2,y2);
 end;
 gotoXY(x,y);
 if found
 then writeLn('TSRs detected!')
 else writeLn('No TSRs are hooked...');
 c := readkey; if c = #0 then Readkey;
 window (1,1,80,viewerYMax);
end;

procedure makeFrame2(f : frame; cs : charSet);
var b : byte;
begin
with f do begin
with cs do begin
 gotoXY (x1-1,y1-1);
 textColor(color[3]); write (luTurn);
 textColor(color[2]); write(hline);
 textColor(color[1]); for b := 0 to x2-x1-2 do write(hline);
 textColor(color[2]); write(hline);
 textColor(color[3]); write (RuTurn);
 textColor(color[2]);
 gotoXY (x1-1,whereY+1); write(vline);
 gotoXY (x2+1,whereY);   write(vline);
 textColor(color[1]);
 for b := 0 to y2-y1-2 do begin
  gotoXY (x1-1,whereY+1); write(vline);
  gotoXY (x2+1,whereY);   write(vline);
 end;
 textColor(color[2]);
  gotoXY (x1-1,whereY+1); write(vline);
  gotoXY (x2+1,whereY);   write(vline);
 gotoXY (x1-1,whereY+1);
 textColor(color[3]); write(ldTurn);
 textColor(color[2]); write(hline);
 textColor(color[1]); for b := 0 to x2-x1-2 do write(hline);
 textColor(color[2]); write(hline);
 textColor(color[3]); write (RdTurn);
 window(x1,y1,x2,y2);
 clrScr;
 window(1,1,80,viewerYmax);
end;
end;
end;

procedure verticalLine (x1,y,x2, color : byte; cs : charSet);
var b : byte;
begin
 textAttr := cs.color[color];
 gotoXY(x1,y);
 for b := x1 to x2 do write(cs.hline);
end;


procedure txtBox (x,y : byte; txtColor : byte; txt : String60; cs : CharSet);
var b : byte;
begin
 gotoXY (x,y);
 textAttr := txtColor;
 write  (' '+txt+' ');
 textAttr := cs.color[2];
 gotoXY (x-1,y);
 write (cs.lSplit);
 gotoXY (x,y-1);
 write (cs.hline);
 gotoXY (x,y+1);
 write (cs.hline);
 gotoXY (x+2+length(txt),y);
 write (cs.rSplit);
 gotoXY (x+1+length(txt),y-1);
 write (cs.hline);
 gotoXY (x+1+length(txt),y+1);
 write (cs.hline);
 textAttr := cs.color[3];
 gotoXY (x-1,y-1);
 write (cs.luTurn);
 gotoXY (x-1,y+1);
 write (cs.ldTurn);
 gotoXY (x+2+length(txt),y-1);
 write (cs.ruTurn);
 gotoXY (x+2+length(txt),y+1);
 write (cs.rdTurn);

 textAttr := cs.color[1];
 for b := x+1 to x+length(txt) do begin
  gotoXY (b,y-1);
  write  (cs.hline);
  gotoXY (b,y+1);
  write  (cs.hline);
 end;
end;

procedure displayField (fieldno : byte);
var a,b : byte; s : string40;
begin
b:= 1;
with mf.menuF[fieldno] do
 while entry[b].name[1] <> #0 do begin
  if selected <> b then begin
   textColor (mf.cNotF);
   textBackground (mf.cNotB);
  end else begin
   textColor (mf.cSelF);
   if mf.currentF = fieldno
   then textBackground (mf.cSelB)
   else textBackground (mf.cNotB);
  end;
  s := entry[b].name;
  while length(s) < (frm.x2-frm.x1+1) do begin
   s := s+' ';
   if length(s) < (frm.x2-frm.x1+1) then s := ' '+s;
  end;
  gotoXY (frm.x1,frm.y1+b-1);
  write  (s);
  inc(b);
 end;
 s := '';
 with mf.menuF[fieldno] do begin
  while length(s) < (frm.x2-frm.x1+1) do begin
   s := s+' ';
   if length(s) < (frm.x2-frm.x1+1) then s := ' '+s;
  end;
  textBackground (mf.cNotB);
  for b := b to 10 do begin
   gotoXY (frm.x1,frm.y1+b-1);
   write  (s);
  end;
 end;
end;

procedure programBAR (y,pColor, mColor : byte; txt : string60; cs : charSet; dispMenuInfo : boolean);
begin
verticalLine (1, y+1,80,1,cs);
txtBox       (4, y+1,pColor,txt,cs);
if dispMenuInfo then begin
 txtBox       (22,y+1,mColor,menuInfo,cs);
 txtBox       (40,y+1,mColor,website,cs);
end;
end;



procedure viewerBeep;
begin
 bell(50,300);
end;

procedure viewerByteArray2Array (p : array of byte; len : word);
var
 w : word;
 strNum : word;
 s : string;
begin
 viewerLineCount := 0;
 s := '';
 for w := 0 to len-1 do
  case p[w] of
     9: begin
         s := s + ' ';
         while (integer(length(s)) div 8) <> (integer(length(s)) / 8) do s := s+' ';
        end;
   013: begin
         LST_add2list(viewerLinePtr,s);
         inc(viewerLineCount);
         s := '';
        end;
   032..255:
    if length(s) < 79
    then s:= s + char(p[w])
    else begin
     LST_add2list(viewerLinePtr,s);
     inc(viewerLineCount);
     s := char(p[w]);
    end;
  end;
  LST_add2list(viewerLinePtr,s);
  inc(viewerLineCount);
end;

procedure viewerPointer2Array (p : parrayofbyte; len : word);
var
 w : word;
 strNum : word;
 s : string;
 b : byte;
begin
 viewerLineCount := 0;
 s := '';
{ case p^[0] of
  13, 10: b:= 0;
  else b := 1;
 end;}
 for w := 1 to len do
  case p^[w] of
     9: begin
         s := s + ' ';
         while (integer(length(s)) div 8) <> (integer(length(s)) / 8) do s := s+' ';
        end;
   013: begin
         LST_add2list(viewerLinePtr,s);
         inc(viewerLineCount);
         s := '';
        end;
   032..255:
    if length(s) < 79
    then s:= s + char(p^[w])
    else begin
     LST_add2list(viewerLinePtr,s);
     inc(viewerLineCount);
     s := char(p^[w]);
    end;
  end;
  LST_add2list(viewerLinePtr,s);
  inc(viewerLineCount);
end;


procedure viewerFile2Array (fileName : string80);
var
 f : text;
 s,s2 : string;
 b    : byte;
begin
 viewerLineCount := 0;
 if fileExists (fileName) then begin
  assign(f, filename);
  reset (f);
  while (not eof(f)) and (viewerLineCount < viewerLineMax) and (IOresult = 0) and (LST_OutOfMemory = false) do begin
   inc(viewerLineCount);
   s := '';
   readLn(f,s2);
   for b := 1 to length(s2) do begin
    if s2[b] = #9 then begin
     s := s + ' ';
     while (integer(length(s)) div 8) <> (integer(length(s)) / 8) do s := s+' ';
    end else s := s + s2[b]
   end;
   if byte(s[0]) >= 80 then s[0] := #79;
   LST_add2list (viewerLinePtr, s);
  end;
  close (f);
 end;
end;

procedure viewerDatFile2Array (fileName : string80; no : byte; startOffset : longInt);
var
 w   : word;
 s   : string;
 p   : array [1..2*k] of byte;
 Cnt : word;

begin
 viewerLineCount := 0;
 s := '';
 assign(dat_fv, fileName);
 reset (dat_fv, 1);
 seek  (dat_fv, startOffset);
 dat_seek (no);
repeat
 blockread(dat_fv, p, sizeOf(p), cnt);
 if (filePos(dat_fv) > startOffset+dat.dl) then Cnt := longInt(Cnt) + startOffset+dat.dl - filePos(dat_fv);
 for w := 0 to cnt do
  case p[w] of
   013: begin
         LST_add2list(viewerLinePtr,s);
         inc(viewerLineCount);
         s := '';
        end;
   032..255:
    if length(s) < 79
    then s:= s + char(p[w])
    else begin
     LST_add2list(viewerLinePtr,s);
     inc(viewerLineCount);
     s := char(p[w]);
    end;
  end;
until (filePos(dat_fv) >= startOffset+dat.dl);
 LST_add2list(viewerLinePtr,s);
 inc(viewerLineCount);
 close(dat_fv);
end;



procedure viewerScrUpdate;
var w : word;
    emptyString : string;
begin
 textColor(viewerTextColor);
 with viewerScrTxt do begin
  window (x1,y1,x2,y2);
  emptyString := paddStrRight('',#32,79);
  LST_GotoItem (viewerLinePtr, viewerLineCurrent);
  for w := 0 to y2-y1 do begin
   gotoxy(1,w+1);
   if viewerLineCurrent+w <= viewerLineCount
   then begin
    write(paddStrRight(viewerLinePtr^.s,#32,79));
    LST_NextItem(viewerLinePtr);
   end else write(emptyString);
  end;
 end;
 textColor(viewerCntColor);
 with viewerScrCnt do begin
  window (x1,y1,x2,y2);
  gotoXY (1,1);
  if viewerHexCnt
  then write(hexWord(viewerLineCurrent))
  else write(abc(viewerLineCurrent,4));
 end;
 window (1,1,80,viewerYMax);
end;


function getErrorMsg (errCode : integer) : string;
begin;
  case errCode of
   003: getErrorMsg := 'Path not found';
   004: getErrorMsg := 'Too many open files';
   005: getErrorMsg := 'Access denied';
   006: getErrorMsg := 'Invalid file handle';
   008: getErrorMsg := 'Out of memory';
   009: getErrorMsg := 'Memory error';
   015: getErrorMsg := 'Invalied drive';
   019: getErrorMsg := 'Write-protected disk';
   021: getErrorMsg := 'Drive not ready';
   027: getErrorMsg := 'Sector not found';
   028: getErrorMsg := 'Printer out of paper';
   029: getErrorMsg := 'Write fault';
   032: getErrorMsg := 'Sharing violation';
   033: getErrorMsg := 'Lock violation';
   101: getErrorMsg := 'Not enough space';
   150: getErrorMsg := 'Write-protected disk';
   152: getErrorMsg := 'No disk in drive';
   160: getErrorMsg := 'Device not found';
   else getErrorMsg := 'Unexpected Error';
  end;
end;

procedure viewerPrint ( media : string );
var f : text; w : word; s : string;
const newPaper : char = #12;
begin
 {$I-}
 assign  (f, media);
 rewrite (f);
 viewerIOR := IOResult;
 LST_gotoFirstItem (viewerLinePtr);
 if viewerIOR = 0 then begin
  for w := 1 to viewerLineCount do
   if viewerIOR = 0 then begin
    writeLn(f,viewerLinePtr^.s);
    viewerIOR := IOResult;
    LST_NextItem (viewerLinePtr);
   end;
  if viewerIOR = 0 then begin
   write(f, newPaper);
   viewerIOR := IOResult;
  end;
  close(f);
  if viewerIOR = 0
  then viewerIOR := IOResult
  else w := IOResult;
 end;
 {$I+}
 if viewerIOR <> 0 then begin
  viewerBeep;
  viewerScrUpdate;
  makeFrame2(viewerPrinterFrame,chStd);
  textColor (lightred);
  gotoxy(1,viewerPrinterFrame.y1+1);
  centerWriteLn (40,'WARNING!');
  centerWriteLn (40,'Error '+abc(viewerIOR,0)+': '+getErrorMsg(viewerIOR));
  readkey;
 end;
end;



procedure viewerInit (cs : charSet);
var x : byte; s : string;
procedure subProcCNTX (y,color : byte; msg : string);
begin
 if x <> 0
 then x := x+4
 else x := 3;
 txtBox (x, y,cs.color[color],msg,cs);
 x := x+length(msg);
end;

begin
 timetask.init;
 if viewerResetMode then begin
  case viewerYmax of
   25: textMode (CO80);
   43,
   50: textMode(CO80 + Font8x8);
  end;
  viewerLastMode    := viewerLastMode;
 end;
 gotoXY(1,25);
 gotoXY(1,43);
 gotoXY(1,50);
 viewerYmax := whereY;

 viewerLineCurrent := 1;
 clrScr;
 turnCursor(Off);
 verticalLine (1, 2,80,1,cs);
 x := 0;
 if viewerInfF <> '' then subProcCNTX (2,3,viewerInfF);
 if viewerInfS <> '' then subProcCNTX (2,3,viewerInfS);
 if viewerInfA <> '' then subProcCNTX (2,3,'Author: '+viewerInfA);
 verticalLine (1, viewerYmax-1,80,1,cs);

 begin
  s := hexWord(viewerLineCurrent)+' / '+hexWord(viewerLineCount);
  x := 3;
  txtBox (x,viewerYmax-1,viewerCntColor,s,cs);
  x := x+length(s);
 end;
{ x := 14;}
 subProcCNTX (viewerYmax-1,1,viewerID);
 subProcCNTX (viewerYmax-1,1,'PgUp/PgDn/Home/End');
 subProcCNTX (viewerYmax-1,1,'S: Save');
 subProcCNTX (viewerYmax-1,1,'H: Help');
 with viewerScrCnt do begin
  x1 := 4;
  x2 := 4+4+3+4;
  y1 := viewerYmax-1;
  y2 := viewerYmax-1;
 end;
 with viewerScrTxt do begin
  x1 := 1;
  x2 := 80;
  y1 := 4;
  y2 := viewerYmax-3;
 end;
 with viewerPrinterFrame do begin
  y1 := (viewerYMax div 2);
  y2 := y1+3;
  x1 := 40-17;
  x2 := 40+17;
 end;
 with viewerInfoFrame do begin
  y1 := (viewerYMax div 2)-3;
  y2 := y1+10;
  x1 := 40-30;
  x2 := 40+30;
 end;
end;

function isGod : byte;
var
 done,
 finished : boolean;
 c : char;
 s : string[10];
 b : byte;
const
 godMax = 4;
var
 godArr : array [1..godMax] of string[10];
begin
 godArr[1] := 'BELLDANDY';
 godArr[2] := 'URD';
 godArr[3] := 'SKULD';
 godArr[4] := 'TSRSEEK';

 isGod := 0;
 s := '';
 finished := false;
 Repeat
   done := true;
   s := s + upcase(readkey);
   for b := 1 to godmax do
    if (s = copy(godArr[b],1,length(s))) then done := false;
   for b := 1 to godmax do
    if s = godArr[b] then begin
     isGod := b;
     finished := true;
    end;
 until done or finished;
 while keypressed do readkey;
end;



procedure viewerMainRoutine;
var
 c : char;
 b : byte;
 w : word;
 s : string;
 vlc,
 y2y1 : longint;
begin
repeat
 vlc := viewerLineCount - (viewerScrTxt.y2-viewerScrTxt.y1);
 window (1,1,80,viewerYmax);
 while not keypressed do begin
  timeslice;
 end;
 c := readkey;
 case upcase(c) of
  #10: begin
        case isGod of
         1: begin
            makeFrame2(viewerPrinterFrame,chStd);
            LightBarTextScroll_Double
               (40,viewerPrinterFrame.y1+1,
               'Sorry, there was no answer','Please, Try 1-555-Goddess!',
               white,yellow,lightred, red, 80);
            end;
         2: begin
            makeFrame2(viewerPrinterFrame,chStd);
            LightBarTextScroll_Double
               (40,viewerPrinterFrame.y1+1,
               'Beware man as well as divine!','  -: Terrible Master Urd :-  ',
               white,yellow,lightred, red, 30);
            end;
         3: begin
            makeFrame2(viewerPrinterFrame,chStd);
            textColor (red+blink);
            gotoxy(1,viewerPrinterFrame.y1+1);
            centerWriteLn (40,'WARNING!');
            LightBarTextScroll (25,whereY,'The Heavenly Hacker is online!',white,yellow,lightred, red, 40);
            end;
         4: TSRInfoSeek;
        end;
        viewerScrUpdate;
       end;
  'M': begin
        makeFrame2(viewerPrinterFrame,chStd);
        textColor (white);
        gotoxy(1,viewerPrinterFrame.y1+1);
        centerWriteLn (40,'Available Memory:');
        centerWrite   (40,abc(MemAvail,0));
        if readkey = #0 then readkey;
        viewerScrUpdate;
       end;
  'X': begin
        viewerHexCnt := viewerHexCnt xor true;
        gotoXY (4,viewerYmax -1);
        if viewerHexCnt
        then write(hexWord(viewerLineCurrent)+' / '+ hexWord(viewerLineCount))
        else write(abc(viewerLineCurrent,4) + ' / '+ abc(viewerLineCount,4));
       end;
  'H': begin
        makeFrame2(viewerInfoFrame,chStd);
        textColor (white);
        gotoxy(1,viewerInfoFrame.y1+1);
        centerWriteLn (40,'Eleventh View Help');
        writeLn;
        textColor(green);
        gotoxy(38,whereY); writeLn ('[ Esc  ] Exit viewer');
        gotoxy(38,whereY); writeLn ('[ Home ] Jump to start');
        gotoxy(38,whereY); writeLn ('[ End  ] Jump to end');
        gotoxy(38,whereY); writeLn ('[ Up   ] Scroll Upwards');
        gotoxy(38,whereY); writeLn ('[ Down ] Scroll Downwards');
        gotoxy(38,whereY); writeLn ('[ PgUp ] Scroll Page Upwards');
        gotoxy(38,whereY); writeLn ('[ PgDn ] Scroll Page Downwards');
        gotoxy(1,viewerInfoFrame.y1+3);
        gotoxy(15,whereY); writeLn ('[S] Save text');
        gotoxy(15,whereY); writeLn ('[P] Print text');
        gotoxy(15,whereY); writeLn ('[X] Hex/Dec counter');
        gotoxy(15,whereY); writeLn ('[M] Memory');
        textColor(lightgreen);
        gotoxy(15,whereY); writeLn ('[I] Information');
        gotoxy(15,whereY); writeLn ('[C] CPU/Stack');
        if readkey = #0 then readkey;
        viewerScrUpdate;
       end;
  'P': begin
        makeFrame2(viewerPrinterFrame,chStd);
        textColor (white);
        gotoxy(1,viewerPrinterFrame.y1+1);
        centerWriteLn (40,'Select Printer:');
        centerWrite   (40,'LPT1 LPT2 LPT3 LPT4');
        viewerBeep;
        repeat
         c := readkey;
         case c of
          #0: Readkey;
          '1'..'4':
           begin
            viewerPrint('LPT'+c);
            viewerScrUpdate;
            break;
           end;
         end;
        until c = #27;
        viewerScrUpdate;
       end;
  'S': begin
        makeFrame2(viewerPrinterFrame,chStd);
        textColor (white);
        gotoxy(1,viewerPrinterFrame.y1+1);
        centerWriteLn (40,'Enter File name:');
        viewerBeep;
        s := viewerInfF;
        turnCursor(on);
        repeat
         centerWrite (40,' '+s+' ');
         gotoxy (whereX-1,whereY);
         c := readkey;
         case c of
          '<', '>', '+', ',', '/', '?', '*': ;
          #0: readkey;

          #8:  if length(s) > 0 then s[0] := char(length(s)-1);
          ':': if length(s) = 1 then s := s+c;
          '\': if s[length(s)] <> '\' then s := s+c;
          #33..#128:
            with viewerPrinterFrame do
             if length(s) < (x2-x1-2) then s := s+c;
          #13: begin
            turnCursor(Off);
            if length(s) > 3
            then viewerPrint(s)
            else viewerBeep;
            viewerScrUpdate;
            break;
          end;
         end;
        until c = #27;
        turnCursor(Off);
        viewerScrUpdate;
       end;
  #27:
   begin
    LST_Del_List(viewerLinePtr);
    if viewerResetMode then textMode(viewerLastMode);
    delay(100);
    while keypressed do readkey;
    exit;
   end;
  #00:
   begin
    c := readkey;
    w := viewerLineCurrent;
    case c of
     #72: dec(viewerLineCurrent);
     #80: inc(viewerLineCurrent);
     #81: if viewerLineCurrent+viewerScrTxt.y2-viewerScrTxt.y1 < vlc
          then viewerLineCurrent := viewerLineCurrent+viewerScrTxt.y2-viewerScrTxt.y1
          else viewerLineCurrent := vlc;
     #73: if viewerLineCurrent > (viewerScrTxt.y2-viewerScrTxt.y1)
          then viewerLineCurrent := viewerLineCurrent-(viewerScrTxt.y2-viewerScrTxt.y1)
          else viewerLineCurrent := 1;
     #79: viewerLineCurrent := vlc;
     #71: viewerLineCurrent := 1;
    end;
    if viewerLineCurrent < 1 then viewerLineCurrent := 1;
    if viewerLineCurrent > vlc then viewerLineCurrent := vlc;
    if vlc < 1 then viewerLineCurrent := 1;
    if w <> viewerLineCurrent then begin
     viewerScrUpdate;
     vlc := viewerLineCount - (viewerScrTxt.y2-viewerScrTxt.y1);
    end;
   end;
 end;
until false;
end;

end.