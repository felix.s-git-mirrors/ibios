
unit view11al;
{$M 3072,0,655360}
                      {----------------------------------}
                      {  Written by K.Lundahl  96-01-14  }
                      {  Edited by P.Magnusson 96-01-14  }
                      {----------------------------------}
interface
USES
  Crt;
TYPE
  TPList = ^TList ;
  TList = RECORD
    S    : String [79];
    Prev : TPList ;
    Next : TPList ;
  END ;
Const
 LST_OutOfMemory : Boolean = False;

PROCEDURE LST_NextItem( VAR L : TPList ) ;
PROCEDURE LST_PrevItem( VAR L : TPList ) ;
PROCEDURE LST_GoToFirstItem( VAR L : TPList ) ;
PROCEDURE LST_GoToLastItem ( VAR L : TPList ) ;
PROCEDURE LST_GoToItem( VAR L : TPList; n : Word) ;
PROCEDURE LST_Add2list( VAR L : TPList; S : string ) ;
PROCEDURE LST_DispList( L : TPList ) ;
PROCEDURE LST_DEL_Item( VAR L : TPList) ;
PROCEDURE LST_DEL_List( VAR L : TPList) ;

IMPLEMENTATION
PROCEDURE LST_NextItem( VAR L : TPList ) ;
BEGIN
  if L^.Next <> Nil then L := L^.Next
END ; {- NextItem -}

PROCEDURE LST_PrevItem( VAR L : TPList ) ;
BEGIN
  if L^.Prev <> Nil then L := L^.Prev
END ; {- PrevItem -}

PROCEDURE LST_GoToFirstItem( VAR L : TPList ) ;
BEGIN
  WHILE L^.Prev <> NIL DO BEGIN
    LST_PrevItem( L )
  END ;
END ; {- GoToFirstItem -}

PROCEDURE LST_GoToLastItem( VAR L : TPList ) ;
BEGIN
  WHILE L^.Next <> NIL DO BEGIN
    LST_NextItem( L )
  END ;
END ;


PROCEDURE LST_GoToItem(VAR L : TPList;  n : Word ) ;
BEGIN
  LST_GoToFirstItem( L ) ;
  FOR n := 1 TO n-1 DO BEGIN
    LST_NextItem( L ) ;
  END ;
END ; {- GoToItem -}

PROCEDURE LST_Add2list( VAR L : TPList ; S : string ) ;
VAR
  Tmp : TPList ;
BEGIN
 if memAvail < 2000 then begin
   LST_OutOfMemory := true;
 end else begin
    LST_OutOfMemory := false;

    New( Tmp ) ;
    Tmp^.S :=    S   ;
    Tmp^.Next := NIL ;   {-  See for explanation after "END."  -}
    Tmp^.Prev := L   ;

    if L <> Nil then begin
     LST_GotoLastItem( L );
     L^.Next := Tmp;
     LST_NextItem( L );
    end else L := Tmp;
 end;
END ; {- Add2list -}

PROCEDURE LST_DEL_Item( VAR L : TPList) ;
VAR
  Tmp,
  TmpPrev,
  TmpNext  : TPList ;
BEGIN
    Tmp := L;
    TmpPrev := Tmp^.Prev;
    TmpNext := Tmp^.Next;
    If (TmpPrev <> Nil) then Tmp^.Prev^.Next := TmpNext;
    If (TmpNext <> Nil) then Tmp^.Next^.Prev := TmpPrev;
    dispose (Tmp);
    If (TmpNext <> Nil)
    then L := TmpNext
    else
      If (TmpPrev <> Nil)
      then L := TmpPrev
      else L := Nil;
END;

PROCEDURE LST_DEL_List( VAR L : TPList );
BEGIN
    while L^.Prev <> Nil do L := L^.Prev;
    while L^.Next <> Nil do begin
     L := L^.Next;
     dispose (L^.Prev);
    end;
    if L <> Nil then dispose(L);
    L := Nil;
END;

PROCEDURE LST_DispList( L : TPList ) ;
CONST
  LF = #10 ;
BEGIN

    WriteLn( '--------------------------------------------' ) ;
    WriteLn( 'Displaying all allocated items. Mem = ', MemAvail ) ;
    WriteLn( '--------------------------------------------' ) ;

  IF L <> NIL THEN BEGIN

    LST_GoToFirstItem( L ) ;
    WriteLn( L^.S ) ;
    WHILE L^.Next <> NIL DO BEGIN
      LST_NextItem( L ) ;
      WriteLn( L^.S ) ;
    END ;
    WriteLn( '--------------------' ) ;
  END ;
END ; {- DispList -}

END.

Explanation for: PROCEDURE Add2list( VAR L : TPList ; I : Word ) ;

  Suppose as an example that 1 item is allocated.

           �����Next �> NIL
           �   L   �
    NIL <� Prev�����
  .
  .
                          �����Next �> NIL
  Tmp^.Next := NIL ;      �  Tmp  �
                          ���������

                          �����Next �> NIL
  Tmp^.Prev := L ;        �  Tmp  �
                     L <� Prev�����

                          �����Next �> Tmp
  L^.Next := Tmp ;        �   L   �
                   NIL <� Prev�����

  Now the picture is this...

           �����Next �>  �����Next �> NIL
           �   L   �     �  Tmp  �
    NIL <� Prev�����  <� Prev�����

                           �����Next �>  �����Next �> NIL
  NextItem( L ) ;          �       �     �   L   �
                    NIL <� Prev�����  <� Prev�����
  .
  .
  Himla bra!!! - Hurra!!!

