@echo off

REM *** Part 1: Informational message ***

cls
type IBIOSS.TXT
echo P1.
pause

REM *** Part 2: Load settings and do some other funky stuff ***

call SETTINGS.BAT
mkdir %IB_WORK% > NUL:
del BIOS320.EXE
echo P2.
pause

REM *** Part 3: Copy various files to %IB_WORK% ***
call BIGCOPY.BAT
echo P3.
pause

REM *** Part 4: Prepeare built-in documents ***

call MAKEDOC.BAT
echo P4.
pause

REM *** Part 5: Compilation ***

cd %IB_WORK%
REM %IB_COMPILER% -DSCk -DNDM -DAlphaTest -DCheckOS BIOS320.PAS
%IB_COMPILER% -DSCk -DNDM -DCheckOS BIOS320.PAS
echo P5.
pause

REM *** Part 5: Patching ***

echo Runtime 200 patch (crt::delay fix)
%IB_UTILS%\TPPATCH.EXE BIOS320.EXE
echo Packing file using PkLite
%IB_UTILS%\PKLITE.EXE BIOS320.EXE
echo Adding selfcheck checksum to EXE
%IB_UTILS%\PMPKP.EXE BIOS320.EXE GOO!
echo P6.
pause

REM *** Finito ***

cd %IB_DIR%
COPY %IB_WORK%\BIOS320.EXE
COPY BIOS320.EXE IBIOS.EXE


echo.
echo Hopefully the compile suceeded... ;-)
echo.

echo FYI: CMOS.LST is in the DOCS directory. 
echo It will enhance the editor if kept in the same directory as BIOS320.EXE
echo.
