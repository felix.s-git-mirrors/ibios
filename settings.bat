set IB_DIR=D:\11A\IBIOS\SRC\DOS\unpacked

set IB_UTILS=%IB_DIR%\UTILS\BIN
set IB_WORK=%IB_DIR%\WORK
set IB_DOCS=%IB_DIR%\DOCS

set IB_BP=D:\BP
set IB_BPU=%IB_WORK%
set IB_BPE=%IB_WORK%

set IB_COMPILER=D:\BP\BIN\BPC.EXE -B -CD -$G+ -U. -E.

REM Borland Pascal Compiler (BPC) Options
REM *************************************
REM -B   = Build all units (clean compile)
REM -CD  = Create Dos Realmode .EXE
REM -$G+ = Support 80286 assembly by default
REM -U   = Unit directory/directories
REM -E   = EXE/TPU directory
