{$F+}
uses apps,crt;
var
  f       : file;
  pasF,
  cppF,
  asmF,
  incF,
  debF    : text;
  b,err   : byte;
  numread : integer;
  c       : char;
  s       : string;
  arr     : array [1..15] of byte;

function name(ext : string) : string;
var s : string;
    b : byte;
label done;
begin
 s := paramStr(1);
 for b:=length(s) downto 1 do begin
  case s[b] of
   '\','.': goto done;
  end;
 end;
 done:
 if s[b] = '.' then s := copy (s,1,b) + ext
 else s := s + '.'+ ext;
{ writeln (s);
 halt(0);}
 name := upcase_s(s);
end;

procedure overYN (name : string);
begin
 if upcase_s(paramStr(1)) = name then begin
  writeLn ('FATAL ERROR: If continued file ['
   +upcase_s(paramStr(1))+'] could have been destroyed!'+rad+
   +'ASM/PAS/C files must be renamed before executing BinConv!');
  bell(600,900);
  halt(20);
 end;
 write('Warning! File [',name,'] Exists! Overwrite? [y/N] ');
 repeat
  repeat until keypressed;
  c := readkey;
  case upcase(c) of
   #13,#27,'N': begin
    WriteLn ('No, terminate.');
    close(f);
    halt(1);
   end;
   'Y': writeLn ('Yes');
  end;
 until upcase(c) = 'Y';
end;

procedure open (var textf : text; filnamn : string);
begin
 assign (textf,filnamn);
 if IOResult <> 0 then begin
  writeLn ('FATAL ERROR: Error while opening (assign) file [',filnamn,']');
  bell(600,900);
  halt(10);
 end;
 rewrite(textf);
 if IOResult <> 0 then begin
  writeLn ('FATAL ERROR: Error while opening (reWrite) file [',filnamn,']');
  bell(600,900);
  halt(10);
 end;
end;

begin
{clrScr;}
err := 0;
writeLn ('BINCONV Beta 1.4 � Freeware (c) Peter Magnusson 2:203/253@Fidonet');
if (paramCount <> 1)
or not fileExists(paramStr(1)) then begin
 writeLn (
 +'Converter of Binary files into Assembler/Pascal/C arrays, Usage:'+rad
 +'BinConv <Binary.Bin>');
 if ((not fileExists(paramStr(1)) and (paramCount = 1))) then begin
  writeLn (rad+'FATAL ERROR: File [',upcase_s(paramStr(1)),'] does not exist!');
  bell(600,900);
 end;
 halt(1);
end;
assign(f, paramStr(1));
reset (f, 1);

if (fileSize(f) = 0) then begin
 writeLn('FATAL ERROR: File [',upcase_s(paramStr(1)),'] is empty.');
 bell(600,900);
 halt(1);
end;
if (fileSize(f) >= 64*k) then begin
 writeLn (
 'FATAL ERROR: File [',upcase_s(paramStr(1)),'] is to big!'+rad+
 'Files larger than 64k will be useless (none working) in source');
 bell(600,900);
 halt(1);
end;

if FileExists(name('CPP')) then overYN (name('CPP'));
if FileExists(name('PAS')) then overYN (name('PAS'));
if FileExists(name('ASM')) then overYN (name('ASM'));
if FileExists(name('INC')) then overYN (name('INC'));
if FileExists(name('DEB')) then overYN (name('DEB'));

open (cppF,name('CPP'));
open (pasF,name('PAS'));
open (asmF,name('ASM'));
open (incF,name('INC'));
open (debF,name('DEB'));

writeLn(pasF,'{Pascal file by BinConv Freeware (c) Peter Magnusson 2:203/253@Fidonet}');
writeLn(pasF,'{Original file: ',paramStr(1),'}');
writeLn(pasF,'Const BINCODE : Array [1..',fileSize(f),'] of byte = (');
writeLn(cppF,'/* C/C++ file by BinConv Freeware (c) Peter Magnusson 2:203/253@Fidonet */');
writeLn(cppF,'/* Original file: ',paramStr(1),' */'
        +rad+'#define       BINCODELEN = ',fileSize(f),';'
        +rad+'unsigned char BINCODE [] = {');
writeLn(asmF,'; ASM file by BinConv Freeware (c) Peter Magnusson 2:203/253@Fidonet'
        +rad+'; Original file: ',paramStr(1)
        +rad+'BINCODELEN dw ',fileSize(f));
write(asmF,'BINCODE db ');
write(incF,'db ');
write(debF,'n '+upcase_s(paramStr(1))+rad+'a 100'+rad+'db ');
writeLn('Processing '+abc(filesize(f) div 1024,0)+' kB');
repeat
 write('.');
 blockread (f, arr, sizeOf(arr), numread);
 for b:= 1 to numread do begin
  write(cppF,     arr[b]:3);
  write(pasF, '$'+hexbyte(arr[b]));
  write(asmF,     hexbyte(arr[b])+'h');
  write(incF,     hexbyte(arr[b]));
  write(debF,     hexbyte(arr[b]));
  if b < numRead
  then begin
   write(pasF,',');
   write(cppF,',');
   write(asmF,',');
   write(incF,',');
   write(debF,',');
   end
  else
  if not eof(f) then begin
   begin
    write(pasF,','+rad);
    write(cppF,','+rad);
    write(asmF,rad+'db ');
    write(incF,rad+'db ');
    write(debF,rad+'db ');
   end;
  end;{ else}
 end;
until (NumRead = 0);
writeLn(pasF,');');
writeLn(cppF,'};');
writeLn(asmF);
writeLn(incF);
writeLn(debF,rad+rad+'RCX'+rad+hexWord(fileSize(f))+rad+'W'+rad+'Q'+rad+'.');
close(f);
close(pasF);
close(cppF);
close(asmF);
close(incF);
close(debF);
writeLn;
end.
