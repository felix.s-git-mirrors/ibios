uses apps;
var
 p : array [1..5] of string[100];
 b : byte;
 f,
 f2 : text;
 fs : file of char;
 s  : string;
begin
 for b := 1 to 5 do p[b] := upcase_s(paramStr(b));
 if p[1] = 'INIT' then begin
  if paramCount <> 2 then begin
   writeLn ('Parameter error!');
   halt;
  end;
  assign  (f, p[2]+'.INT');
  rewrite (f);
  close   (f);
  assign  (f, p[2]+'.IMP');
  rewrite (f);
  close   (f);
  assign  (f, p[2]+'.PAS');
  rewrite (f);
  close(f);
 end else
 if p[1] = 'ADD' then begin
  if paramCount <> 5 then begin
   writeLn ('Parameter error!');
   halt;
  end;
  assign (f, p[2]+'.INT');
  append (f);
  assign (fs, p[4]);
  reset  (fs);
  writeLn(f,
   'const     '+p[3]+'LEN = ',fileSize(fs),';'+rad+
   'procedure '+p[3]+';');
  close(fs);
  close(f);
  assign (f, p[2]+'.IMP');
  append (f);
  writeLn(f,
   '{$L '+p[5]+'}'+rad+
   'procedure '+p[3]+'; external;');
  close(f);
 end else
 if p[1] = 'CREATE' then begin
  if paramCount <> 3 then begin
   writeLn ('Parameter error!');
   halt(0);
  end;
  assign (f, p[2]+'.PAS');
  append (f);
  writeLn (f,
    'unit '+p[3]+';'+rad+rad+
    'interface'+rad);
  assign (f2, p[2]+'.INT');
  reset  (f2);
  while not eof(f2) do begin
   readLn  (f2, s);
   writeLn (f,  s);
  end;
  writeLn (f, rad+'implementation'+rad);
  close   (f2);
  assign  (f2, p[2]+'.IMP');
  reset   (f2);
  while not eof(f2) do begin
   readLn  (f2, s);
   writeLn (f,  s);
  end;
  writeLn (f, rad+'end.');
  close(f);
  close(f2);
 end else
 begin
  writeLn ('Object Maker has 3 usages:');
  writeLn (
   'INIT   UnitFile'+rad+
   'ADD    UnitFile ObjectName BinFile ObjectFile'+rad+
   'CREATE UnitFile UnitName'+rad);
 end;
end.