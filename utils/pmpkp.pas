uses apps,crt,proc11ai;
var
 f      : file of char;
 w      : word;
 c      : char;
 b      : byte;
 s      : string[30];
const
 fast   : boolean = false;
 v      = 0;
 sv     = 07;
 null   : char = #0;
 seven  : char = #7;
 _nem   : string = 'Not enough memory';
 hPMPkP : string = 'PMPkP$'+char(v)+char(sv);
 h11A   : string = 'PROC11A (c) Eleventh Alliance';

function YesOrNo(default : YesNo) : YesNo;
var c : char;
begin
 if Default = No
 then write (' [y/N] ')
 else write (' [Y/n] ');
if not fast then begin
 repeat
  c:= upcase(readkey);
  if c = #0 then readkey;
 until (c = #13) or (c = 'N') or (c = 'Y');
 case c of
  'N' : YesOrNo := No;
  'Y' : YesOrNo := Yes;
  #13 : YesOrNo := Default;
 end;
end else YesOrNo := Default;
end;


begin
 randomize;
 writeLn(rad+'Peter Magnusson PkLite Protect [PMPkP] ',v,'.',abc(sv,2),rad);
 if (paramcount <> 1) then begin
  if (paramCount = 2) and (upcase_s(paramStr(2)) = 'GOO!')
  then fast := true
  else begin
   writeLn ('Usage: PMPkP PkLitedFile'+rad);
   halt(1);
  end;
 end;
 if not fileExists (paramStr(1)) then begin
  writeLn ('! Error: File not found!');
  halt(2);
 end;
 assign  (f, paramStr(1));
 reset   (f);
 s := '��';
 for b := 1 to length(s) do read(f, s[b]);
 if not ((s = 'MZ') or (s = 'ZM')) then begin
  writeLn('! Error: File is not an EXE file!');
  halt(0);
 end;
 seek    (f, $1E);
 for b := 1 to length(s) do read(f, s[b]);
 if s <> 'PK' then begin
  write ('! Warning: PKLite header not found! Continue?');
  if YesOrNo(No) = No
  then begin
   writeLn ('Nope! Terminating...');
   halt(0);
  end;
  writeLn ('Yeah!'+rad);
 end;
 seek  (f, $8);
 read  (f, c);
 w := byte(c);
 read  (f, c);
 w := w + (256*word(c));
 w := w*16;
 writeLn('% Executable Header size: [',hexword(w)+']');
 writeLn('% Analyzing Executable Header ...');
 s := '����';
 seek (f,$14);
 for b := 1 to length(s) do read(f,s[b]);
 if s = #0+#1+char($F0)+char($FF) then begin
  writeLn ('% Supported Executable Header(CS:IP).');
  writeLn ('! Warning: Header modifing doesn''t work with some files!');
  write   ('? Do you which to modify?');
  if YesOrNo(Yes) = No
  then writeLn('Nope!')
  else begin
   seek(f,$14);
   writeLn('Yeah!'+rad+'� Modifying (CS:IP) entry ...');
   s := #0+#2+char($E0)+char($FF);
   for b := 1 to length(s) do write(f, s[b]);
  end;
 end else writeLn ('! Wrong Executable Header(CS:IP). Could not modify!');
 seek  (f, $18);
 writeLn (rad+'� Changing relocation offset ...');
 c := char($20);
 write (f, c);
 write (f, null);
 seek  (f, $1C);
 write   ('? Is this an Eleventh Alliance production?');
 if YesOrNo(Yes) = No
 then begin
  writeLn('Nope!');
  s := hPMPkP;
 end
 else begin
  writeLn('Yeah!');
  s := h11A;
 end;
 write ('� Writing ');
 while   (filepos(f) < w) and not eof(f) do
  case filepos(f) of
   $20: write(f, seven);
   $30: begin
        write (rad+'� Writing header ...'+rad+'� Writing ');
        for b := 1 to length(s) do write (f,s[b]);
        end;
   else begin
    write('.');
    write(f, null);
   end;
  end;
 writeLn (rad+rad+'% Analyzing code ... ');
 seek  (f, w+$D);
 read  (f, c);
 if byte(c) <> $72 then writeLn ('! Error: Wrong PkLite version, code could not be modified.')
 else begin
  write ('? Supported jump routine. Do you which to modify?');
  if YesOrNo(Yes) = Yes then begin
   read (f, char(b));
   writeLn ('Yeah!'+rad+'% Old jump ['+hexbyte(b)+']');
   s := char($60)+char($B4)+char($33)+char($CD)+char($21)+char($61)+char($EB);
   b := b - length(s)+1;
   s := s+char(b);
   writeLn ('� New jump ['+hexbyte(b)+']');
   seek (f, filepos(f)-2);
   for b := 1 to length(s) do write(f, s[b]);
  end else writeLn ('Nope!');
 end;
 s := upcase_s(_nem);
 write (rad+'% Searching ');
 seek  (f, w);
 while (filepos(f) < w+$100) and not eof(f) and (s <> _nem) do begin
  write('.');
  s := copy(s,2,length(s)-1)+'�';
  read (f, s[length(s)]);
  if s = _nem then begin
   write(rad+'? The text ['+_nem+'] was found. Scramble?');
   if YesOrNo(Yes) = Yes then begin
    writeLn  ('Yeah!'+rad+'� Scrambling ...');
    seek (f, filepos(f) - length(s));
    for b := 1 to length(s) do begin
     c := char(random(220));
     write(f, c);
    end;
   end else writeLn ('Nope!');
  end;
 end;
 close(f);
 if s <> _nem then writeLn (rad+'! Warning: Text wasn''t found');
 writeLn ('� Adding PROC11A checksums...');
 proc11aProtect (paramStr(1));
 writeLn (rad+'% Normal Termination');
end.